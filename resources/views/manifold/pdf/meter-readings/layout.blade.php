<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <title>MANIFOLD DIMENSIONS SDN BHD - Meter readings</title>

    <style type="text/css">
        @font-face {
            font-family: "Open Sans Light";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Light.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Regular";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Regular.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans SemiBold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-SemiBold.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Bold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Bold.ttf') }}) format("truetype");
        }

        @page {
            margin: 20mm;
        }

        body {
            margin-top: 35mm;
            margin-bottom: 20mm;
            font-family: "Open Sans Regular", sans-serif;
            color: #404040;
            font-size: 10pt;
            line-height: 3mm;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        .os-reg {
            font-family: "Open Sans Regular", sans-serif;
        }

        .os-light {
            font-family: 'Open Sans Light', sans-serif;
        }

        .os-semi {
            font-family: 'Open Sans SemiBold', sans-serif;
        }

        .os-bold {
            font-family: 'Open Sans Bold', sans-serif;
        }

        .uc {
            text-transform: uppercase;
        }

        .to {
            max-width: 50%;
        }

        .pc {
            margin-bottom: 3mm;
        }

        .reg_number {
            font-size: 80%;
            margin-bottom: 1mm;
        }

        .iv-title {
            margin: 8mm 0 1mm 0;
        }

        .separator {
            height: 3mm;
            width: 100%;
            border-bottom: 0.5pt solid #404040;
            margin-bottom: 6mm;
        }

        .items {
            min-width: 60%;
            line-height: 4.5mm;
            /*page-break-after: always;*/
        }

        .items tr td {
            white-space: nowrap;
            vertical-align: middle;
            padding-right: 10mm;
        }

        .items tr td:last-child {
            padding-right: 0;
        }

        .items tr td img {
            width: 60mm;
            max-width: 60mm;
        }
    </style>

</head>

<body>

@include('manifold.pdf.header')
@include('manifold.pdf.invoice.footer')

<div class="to">
    <div class="pc os-bold">Private & Confidential</div>
    <div class="uc os-semi">{{ $tenant->name }}</div>
    <div class="uc os-semi reg_number">({{ $tenant->reg_number }})</div>
    @if(!empty($tenant->address))
    <div class="uc">{{ $tenant->address }}</div>
    MALAYSIA
    @endif
</div>

<div class="iv-title os-bold">Meter Readings</div>

<table cellspacing="0" cellpadding="0">
    <tr>
        <td>Date&nbsp;&nbsp;&nbsp;</td>
        <td>: {{ $date }}</td>
    </tr>
    <tr>
        <td>Units&nbsp;&nbsp;&nbsp;</td>
        <td>: CUBOPARK {{ $tenant->units_string }}</td>
    </tr>
</table>

<div class="separator"></div>

<table cellspacing="0" cellpadding="0" class="items">
    <tr>
        <td class="os-bold">Date</td>
        <td class="os-bold">Meter</td>
        <td class="os-bold text-right">Reading</td>
        <td class="os-bold text-right">Usage</td>
        <td class="os-bold text-right">Payable <span class="os-reg">(MYR)</span></td>
    </tr>
    @foreach($readings as $reading)
        <tr>
            <td>
                {{ $reading->created_at }}
            </td>

            <td>
                {{ $reading->meter_id }} <span class="os-light">({{ $reading->type }})</span>
            </td>

            <td class="text-right">
                {{ $reading->reading }}
            </td>

            <td class="text-right">
                {{ $reading->usage }}
            </td>

            <td class="text-right">
                {{ $reading->payable }}
            </td>
        </tr>
    @endforeach
</table>

</body>
</html>
