<style type="text/css">
    #footer {
        position: fixed;
        left: 0;
        right: 0;
        bottom: 5mm;
        font-size: 8pt;
        line-height: 2.5mm;
    }

    .no-signature {
        margin-top: 1mm;
        font-size: 5pt;
    }
</style>

<div id="footer">
    <span class="os-bold">{{ config('cubopark.company.name') }}</span>
    <span class="os-light">({{ config('cubopark.company.registration_number') }})</span>
    <br>
    {{ config('cubopark.company.address') }}
    <br>
    T: {{ config('cubopark.company.phone') }}&nbsp;&nbsp;|&nbsp;&nbsp;E: {{ config('cubopark.company.email') }}

    <div class="no-signature">
        This is an electronically generated invoice, no signature is required.
    </div>
</div>

<script type="text/php">
    if (isset($pdf)) {
        $x = $pdf->get_width() - 103;
        $y = $pdf->get_height() - 40;
        $text = "Page {PAGE_NUM} of {PAGE_COUNT}";
        $font = $fontMetrics->get_font('Open Sans Regular, sans-serif', 'normal');
        $size = 9;
        $color = array(0.4,0.4,0.4);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
