<div id="header">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="middle">
                <img src="{{ resource_path('assets/svg/manifold-logo.svg') }}" id="logo">
            </td>
            <td id="company-data" valign="top">
                <span id="company-name">
                    MANIFOLD DIMENSIONS SDN BHD
                    <span>(1182794-H)</span>
                </span>
                <br><br>
                18 Lorong Halia, Tanjung Tokong, 10470 Georgetown, Penang
                <br>
                <span class="md-contact-block">
                    <span>T</span>+60-12-4202285
                </span>
                <span class="md-contact-block">
                    <span>E</span>info@manifolddimensions.com
                </span>
            </td>
        </tr>
    </table>
</div>
