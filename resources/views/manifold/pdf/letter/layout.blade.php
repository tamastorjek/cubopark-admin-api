<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

    <title>MANIFOLD DIMENSIONS SDN BHD</title>

    <style type="text/css">
        @font-face {
            font-family: "Open Sans Light";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Light.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Regular";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Regular.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans SemiBold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-SemiBold.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Bold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Bold.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans ExtraBold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-ExtraBold.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Montserrat";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/Montserrat-SemiBold.ttf') }}) format("truetype");
        }

        @page  {
            margin: 20mm;
        }

        body {
            margin-top: 32mm;
        }

        #header {
            position: fixed;
            left: 0;
            right: 0;
            color: #7A7A7A;
            font-size: 8pt;
            top: 0;
            border-bottom: 0.5pt solid #AA8427;
            padding-bottom: 2mm;
            font-family: 'Open Sans Regular', sans-serif;
            margin-bottom: 10mm;
        }

        #logo {
            width: 29mm;
        }

        #company-data {
            text-align: right;
            white-space: nowrap;
            line-height: 3.5mm;
        }

        #company-name {
            font-family: 'Montserrat', sans-serif;
            font-size: 10pt;
            color: #AA8427;
        }

        #company-name span {
            font-size: 8pt;
            font-family: 'Open Sans Light', sans-serif;
            color: #7A7A7A;
        }

        #company-data .md-contact-block {
            font-family: 'Open Sans SemiBold', sans-serif;
        }

        #company-data .md-contact-block span {
            font-family: 'Open Sans ExtraBold', sans-serif;
            margin-right: 1mm;
        }

        #company-data .md-contact-block:last-child {
            margin-left: 5mm;
        }

        #footer {
            position: fixed;
            left: 0;
            right: 0;
            bottom: 0;
            text-align: right;
            color: #929292;
            font-size: 10pt;
            font-family: 'Open Sans', sans-serif;
            font-weight: 400;
            padding-top: 3mm;
        }

        .content {

        }
    </style>

</head>

<body>

@include('manifold.letter.header')
@include('manifold.letter.footer')

</body>
</html>
