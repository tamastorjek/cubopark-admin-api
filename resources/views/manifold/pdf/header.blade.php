<style type="text/css">
    #header {
        position: fixed;
        left: 0;
        right: 0;
        top: 0;
    }

    #logo {
        width: 30mm;
    }
</style>

<div id="header">
    <img id="logo" src="{{ resource_path('assets/svg/manifold-logo-text.svg') }}">
</div>
