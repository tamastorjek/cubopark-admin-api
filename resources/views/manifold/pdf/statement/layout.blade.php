<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <title>MANIFOLD DIMENSIONS SDN BHD - Statement</title>

    <style type="text/css">
        @font-face {
            font-family: "Open Sans Light";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Light.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Regular";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Regular.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans SemiBold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-SemiBold.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Bold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Bold.ttf') }}) format("truetype");
        }

        @page {
            margin: 20mm;
        }

        body {
            margin-top: 35mm;
            font-family: "Open Sans Regular", sans-serif;
            color: #404040;
            font-size: 10pt;
            line-height: 3mm;
        }

        .os-light {
            font-family: 'Open Sans Light', sans-serif;
        }

        .os-semi {
            font-family: 'Open Sans SemiBold', sans-serif;
        }

        .os-bold {
            font-family: 'Open Sans Bold', sans-serif;
        }

        .uc {
            text-transform: uppercase;
        }

        .to {
            max-width: 50%;
        }

        .pc {
            margin-bottom: 3mm;
        }

        .reg_number {
            font-size: 80%;
            margin-bottom: 1mm;
        }

        .iv-title {
            margin: 8mm 0 1mm 0;
            font-size: 110%;
        }

        .separator {
            height: 3mm;
            width: 100%;
            border-bottom: 0.5pt solid #404040;
            margin-bottom: 6mm;
        }

        .items {
            min-width: 60%;
            line-height: 4.5mm;
            page-break-after: always;
        }

        .text-right {
            text-align: right;
        }

        .currency {
            white-space: nowrap;
        }

        .items thead th {
            vertical-align: top;
        }

        .items thead small {
            font-size: 60%;
            font-style: italic;
            color: #666;
        }

        .items tr td, .items tr th {
            padding-right: 10mm;
            white-space: nowrap;
        }

        .items tr td:last-child, .items tr th:last-child {
            padding-right: 0;
        }

        .total td {
            font-size: 1pt;
            height: 1mm;
            max-height: 1mm;
        }

        .total td:last-child {
            border-bottom: 0.5pt solid #404040;
        }

        .payment-info {
            border: 0.6pt solid #404040;
        }

        .payment-info tr td:first-child {
            padding: 2mm 0 2mm 3mm;
        }

        .payment-info tr td:last-child {
            padding: 2mm 3mm 2mm 0;
        }

        .payment-info tr td.payment-info-text {
            padding: 3mm;
            background: #eeeeee;
        }

        .payment-info tr td.payment-info-colspan {
            padding: 2mm 3mm;
        }

        .payment-info tr td {
            border-top: 0.5pt solid #404040;
        }

        .payment-info tr:first-child td {
            border-top: none;
        }
    </style>

</head>

<body>

@include('manifold.pdf.header')
@include('manifold.pdf.statement.footer')

<div class="to">
    <div class="uc os-semi">{{ $tenant->name }}</div>
    <div class="uc os-semi reg_number">({{ $tenant->reg_number }})</div>
    @if(!empty($tenant->address))
    <div class="uc">{{ $tenant->address }}</div>
    MALAYSIA
    @endif
</div>

<div class="iv-title os-bold">Statement</div>

<table cellspacing="0" cellpadding="0">
    <tr>
        <td>Date&nbsp;&nbsp;&nbsp;</td>
        <td>: {{ $date }}</td>
    </tr>
</table>

<div class="separator"></div>

<table cellspacing="0" cellpadding="0" class="items">
    <thead>
        <tr>
            <th valign="top">INVOICE NO.</th>
            <th valign="top">DATE</th>
            <th valign="top" class="text-right">AMOUNT<br><small>MYR</small></th>
            <th valign="top" class="text-right">AMOUNT PAID<br><small>MYR</small></th>
            <th valign="top" class="text-right">AMOUNT DUE<br><small>MYR</small></th>
        </tr>
    </thead>
    @foreach($invoices as $invoice)
        <tr>
            <td>{{ $invoice->number }}</td>
            <td>{{ $invoice->date }}</td>
            <td class="text-right currency">{{ $invoice->amount_string }}</td>
            <td class="text-right currency">{{ $invoice->paid_amount_string }}</td>
            <td class="text-right currency">{{ $invoice->balance_string }}</td>
        </tr>
    @endforeach
    <tr class="total">
        <td colspan="4">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="os-bold" colspan="4">Total payable (in MYR)</td>
        <td class="os-bold text-right">{{ $total }}</td>
    </tr>
</table>

<!-- PAGE 2 -->
<div class="iv-title os-bold">Statement</div>

<table cellspacing="0" cellpadding="0">
    <tr>
        <td>Date&nbsp;&nbsp;&nbsp;</td>
        <td>: {{ $date }}</td>
    </tr>
</table>

<div class="separator"></div>

<table cellspacing="0" cellpadding="0" class="payment-info">
    <tbody>
    <tr>
        <td colspan="2" class="payment-info-text os-semi">
            Payment due before {{$due}}.
            Interest will be levied on overdue accounts.
            <br>
            Kindly make cheque payable or remit the payment to the following account:
        </td>
    </tr>
    <tr>
        <td>Payee name</td>
        <td class="os-semi">{{ config('cubopark.company.name') }}</td>
    </tr>
    <tr>
        <td>Account number</td>
        <td class="os-semi">{{ config('cubopark.company.account_number') }}</td>
    </tr>
    <tr>
        <td>Beneficiary bank</td>
        <td class="os-semi">{{ config('cubopark.company.bank') }}</td>
    </tr>
    <tr>
        <td>Branch address</td>
        <td class="os-semi">{{ config('cubopark.company.branch') }}</td>
    </tr>
    <tr>
        <td>SWIFT code</td>
        <td class="os-semi">{{ config('cubopark.company.swift') }}</td>
    </tr>
    <tr>
        <td class="os-semi payment-info-colspan" colspan="2">
            <small>
                Please quote your company name and the invoice number(s) on your payment/remittance slip.
            </small>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
