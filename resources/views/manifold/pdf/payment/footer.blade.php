<style type="text/css">
    #footer {
        position: fixed;
        left: 0;
        right: 0;
        bottom: 5mm;
        font-size: 8pt;
        line-height: 2.5mm;
    }

    #footer table {
        margin-top: 1mm;
        vertical-align: bottom;
        width: 100%;
    }

    .no-signature {
        font-size: 5pt;
    }

    .page-number {
        text-align: right;
    }
</style>

<div id="footer">
    <span class="os-bold">MANIFOLD DIMENSIONS Sdn Bhd</span>
    <span class="os-light">(1182794-H)</span>
    <br>
    18 Lorong Halia, Tanjung Tokong, Georgetown, 10470 Penang, Malaysia
    <br>
    T: 012-4202285&nbsp;&nbsp;|&nbsp;&nbsp;E: info@manifolddimensions.com

    <table cellpadding="0" cellspacing="0" border="0">
        <tbody>
        <tr>
            <td class="no-signature">
                This is an electronically generated receipt.
            </td>
            <td class="page-number">Page 1 of 1</td>
        </tr>
        </tbody>
    </table>
</div>
