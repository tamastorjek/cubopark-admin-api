<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <title>MANIFOLD DIMENSIONS SDN BHD - Payment Receipt</title>

    <style type="text/css">
        @font-face {
            font-family: "Open Sans Light";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Light.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Regular";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Regular.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans SemiBold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-SemiBold.ttf') }}) format("truetype");
        }

        @font-face {
            font-family: "Open Sans Bold";
            font-weight: normal;
            font-style: normal;
            src: url({{ storage_path('fonts/OpenSans-Bold.ttf') }}) format("truetype");
        }

        @page {
            margin: 20mm;
        }

        body {
            margin-top: 35mm;
            font-family: "Open Sans Regular", sans-serif;
            color: #404040;
            font-size: 10pt;
            line-height: 3mm;
        }

        .os-light {
            font-family: 'Open Sans Light', sans-serif;
        }

        .os-semi {
            font-family: 'Open Sans SemiBold', sans-serif;
        }

        .os-bold {
            font-family: 'Open Sans Bold', sans-serif;
        }

        .uc {
            text-transform: uppercase;
        }

        .cc {
            text-transform: capitalize;
        }

        .font-size-sm {
            font-size: 75%;
        }

        .font-size-xs {
            font-size: 55%;
        }

        .iv-title {
            margin-bottom: 10mm;
            margin-top: 15mm;
            font-size: 16pt;
        }

        .separator {
            height: 3mm;
        }

        .separator td {
            border-bottom: 0.5pt solid #404040;
        }

        .details tr td {
            vertical-align: top;
            padding-top: 3mm;
        }

        .details tr td:nth-child(2) {
            width: 10mm;
            text-align: right;
            padding-right: 1mm;
        }

        .tenant-name {
            padding-bottom: 2mm;
        }

        .tenant-name .os-light {
            margin-left: 1mm;
        }

        .small-br {
            font-size: 40%;
            line-height: 40%;
        }
    </style>

</head>

<body>

@include('manifold.pdf.header')
@include('manifold.pdf.payment.footer')

<div class="iv-title os-bold uc">Official Receipt</div>

<table cellspacing="0" cellpadding="0" class="details">
    <tr>
        <td class="os-semi">Date</td>
        <td>:</td>
        <td> {{ $payment->date }}</td>
    </tr>
    <tr>
        <td class="os-semi">Receipt Number</td>
        <td>:</td>
        <td> {{ $payment->number }}</td>
    </tr>
    <tr>
        <td class="os-semi">In Payment Of</td>
        <td>:</td>
        <td>
            @foreach($invoices as $invoice)
                @if (!$loop->first)
                    <br>
                @endif

                {{ $invoice->number }}
                @if ($invoice->balance > 0)
                    <span class="os-semi font-size-xs">
                        (Remaining balance: MYR {{ $invoice->balance_string }})
                    </span>
                @endif
            @endforeach
        </td>
    </tr>
    <tr>
        <td class="os-semi">Received From</td>
        <td>:</td>
        <td>
            <span class="uc os-semi tenant-name">{{ $tenant->name }}
            <span class="os-light">({{ $tenant->reg_number }})</span></span>
            @if(!empty($invoice->tenant->address))
                <span class="small-br"><br>&nbsp;<br></span>
                <div class="uc">{{ $invoice->tenant->address }}</div>
                MALAYSIA
            @endif
        </td>
    </tr>
    <tr>
        <td class="os-semi">Payment Method</td>
        <td>:</td>
        <td> {{ config('payment.types')[$payment->type] }}</td>
    </tr>
    <tr>
        <td class="os-semi">Payment Reference</td>
        <td>:</td>
        <td> {{ empty($payment->reference) ? '-' : $payment->reference }}</td>
    </tr>
    <tr class="separator">
        <td colspan="3"></td>
    </tr>
    <tr class="os-bold">
        <td>Amount</td>
        <td>:</td>
        <td><span class="font-size-sm">***</span>
            MYR {{ $payment->amount_string }}
            <span class="font-size-sm">***</span>
            <br><span class="os-light font-size-sm cc">Ringgit Malaysia {{ $payment->amount_words }}</span>
        </td>
    </tr>
    @if($payment->type === 'CHQ')
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="os-light font-size-xs">This receipt is valid subject to bank clearance of the cheque.</td>
        </tr>
    @endif
</table>

</body>
</html>
