<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CUBOPARK</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#5c36f7">
  <meta name="theme-color" content="#ffffff">
<link rel="stylesheet" href="styles.613e29abff3e989712a4.css"></head>

<body>
<app-root>
  <div class="splash">
    <img src="assets/cp_logo_white.svg" alt="CUBOPARK">
  </div>
</app-root>
<script type="text/javascript" src="runtime.26209474bfa8dc87a77c.js"></script><script type="text/javascript" src="es2015-polyfills.8324bb31dd8aa5f2460c.js" nomodule></script><script type="text/javascript" src="polyfills.dc600b650d47ec90afef.js"></script><script type="text/javascript" src="scripts.ca434a2385d09b6d990b.js"></script><script type="text/javascript" src="main.38c65d8df304f7b9a145.js"></script></body>
</html>

