<?php

namespace App\Traits;

use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Tenant;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use js\tools\numbers2words\Speller as NumToWords;

trait PaymentStoreTrait
{

    /**
     * @param Request $request
     * @param array $invoices
     * @return mixed
     */
    public function storePayment(Request $request, array $invoices = [])
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'type' => 'required|string|max:3',
            'reference' => 'string|max:255|nullable',
            'notes' => 'string|max:255|nullable',
            'year' => 'integer|between:17,99',
            'month' => 'integer|between:1,12',
            'created_at' => 'date_format:Y-m-d H:i:s',
            'invoices_paid' => 'json|nullable',
            'proof_file' => 'file|mimes:jpg,jpeg,png,pdf,doc,docx,txt'
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        // get invoices
        if (!$invoices) {
            if (!$request->filled('invoices_paid')) {
                return 'No invoices were provided.';
            }

            $invoices = json_decode($request->input('invoices_paid'), true);
        }

        // check and validate invoices (paid)
        $invoiceCheck = $this->checkInvoices($request->input('amount'), $invoices);

        if ($invoiceCheck !== '') {
            return $invoiceCheck;
        }

        // create year and month values if not passed
        $merge = [];

        if (!$request->filled('year')) {
            if ($request->filled('created_at')) {
                $merge['year'] = Carbon::parse($request->input('created_at'))->format('y');
            } else {
                $merge['year'] = date('y');
            }
        }

        if (!$request->filled('month')) {
            if ($request->filled('created_at')) {
                $merge['month'] = Carbon::parse($request->input('created_at'))->format('n');
            } else {
                $merge['month'] = date('n');
            }
        }

        // get proof file extension
        if ($request->hasFile('proof_file')) {
            $merge['proof_file_ext'] = $request->proof_file->extension();
        }

        // create payment
        $payment = Payment::create(array_merge($request->all(), $merge));

        if (!$payment) {
            return 'Payment was not saved.';
        }

        // link payment to invoice(s)
        $payment->invoices()->attach(array_column($invoices, 'id'));

        // update invoices
        $invoices = $this->updateInvoices($invoices);

        // save proof file
        if ($request->hasFile('proof_file')) {
            $request->proof_file->storeAs('payment_proofs', $payment->file);
        }

        // generate and save invoice
        $this->savePaymentPdf($payment, $invoices);

        return $payment;
    }

    /**
     * $invoices will be updated with `paid_amount` field (passed by reference)
     *
     * @param $amount
     * @param array $invoices
     * @return string
     */
    private function checkInvoices($amount, array &$invoices): string
    {
        // get unpaid invoices with the given IDs
        $results = Invoice::where('paid', false)->find(array_column($invoices, 'id'));

        // not all invoices were found
        if (count($results) !== count($invoices)) {
            return 'Not all invoices were found.';
        }

        // check sum of invoices against amount
        $invoiceTotal = array_sum($results->pluck('amount')->toArray());
        $invoicePartial = array_sum($results->pluck('amount_partial')->toArray());

        // paid more than needed
        if ($invoiceTotal - $invoicePartial < $amount) {
            return 'Payment is more than invoice total.';
        }

        // sort by paid (full payments first)
        usort(
            $invoices,
            function ($a, $b) {
                if ($a['paid'] === $b['paid']) {
                    return 0;
                }

                // either a or b is true, if a is true, return -1 (smaller)
                return $a['paid'] ? -1 : 1;
            }
        );

        foreach ($invoices as &$i) {
            $invoice = $results->find($i['id']);
            $payable = $invoice->amount - $invoice->paid_amount;

            // calculations
            if ($i['paid']) {
                // not enough money
                if ($amount < $payable) {
                    return 'Payment is not enough for the invoice(s).';
                }

                $amount = $amount - $payable;
                $i['paid_amount'] = $invoice->paid_amount + $payable;
                $i['paid_amount_current'] = $payable;

                continue;
            }

            if ($amount >= $payable) {
                return 'Partial invoice can be fully paid';
            }

            if ($amount === 0) {
                return 'Only one partially paid invoice is allowed.';
            }

            $i['paid_amount'] = $invoice->paid_amount + $amount;
            $i['paid_amount_current'] = $amount;
            $amount = 0;
        }

        return $amount === 0 ? '' : 'Something went wrong. Try again.';
    }

    private function updateInvoices(array $invoices): array
    {
        $results = [];

        foreach ($invoices as $i) {
            $invoice = Invoice::find($i['id']);
            $invoice->fill($i)->save();

            $invoice->amount_paid_current = $i['paid_amount_current'];

            $results[] = $invoice;
        }

        return $results;
    }

    /**
     * Save the receipt in PDF
     * Path: storage/app/payments/<Ym>/
     *
     * @param $payment
     * @param $invoices
     */
    public function savePaymentPdf(Payment $payment, array $invoices = [])
    {
        if (!File::isDirectory($payment->pdf_file_path)) {
            File::makeDirectory($payment->pdf_file_path);
        }

        $payment->amount_words = str_replace(' euro', '',
            NumToWords::spellCurrency(
                toFloat($payment->amount),
                NumToWords::LANGUAGE_ENGLISH,
                NumToWords::CURRENCY_EURO,
                true,
                true
            )
        );

        if (!is_object($payment->invoices->first())) {
            return;
        }

        $tenantId = empty($invoices) ? $payment->invoices->first()->tenant_id : $invoices[0]->tenant_id;

        if (empty($invoices)) {
            $invoices = &$payment->invoices;
        }

        PDF::loadView(
            'manifold.pdf.payment.layout',
            [
                'payment' => $payment,
                'tenant' => Tenant::find($tenantId),
                'invoices' => $invoices
            ]
        )->save($payment->pdf_file_path . $payment->pdf_file);
    }
}
