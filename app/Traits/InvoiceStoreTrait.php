<?php

namespace App\Traits;

use App\Models\Invoice;
use App\Models\Tenant;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

trait InvoiceStoreTrait {

    /**
     * @param Request $request
     * @param array $invoices
     * @return mixed
     */
    public function storeInvoice(Request $request)
    {
        if ($request->filled('json')) {
            $request->merge(json_decode($request['json'], true));
        }

        $validator = Validator::make($request->all(), [
            'tenant_id' => 'required|integer|min:1',
            'type' => 'required|string',
            'amount' => 'required|numeric',
            'paid' => 'boolean',
            'year' => 'integer|between:17,99',
            'month' => 'integer|between:1,12',
            'created_at' => 'date_format:Y-m-d H:i:s',
            // items
            'items' => 'required|array|min:1',
            'items.*.title' => 'required|string|max:255',
            'items.*.amount' => 'required|numeric',
            'items.*.comment' => 'string|max:255|nullable'
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        // check if we can generate the invoice
        // if the user has missing data or we don't have anyone to send it to, we stop
        $tenant = Tenant::find($request->input('tenant_id'));

        if (!$tenant->isInvoicable()) {
            return 'Tenant has missing information or no contact info. Please check!';
        }

        $merge = [];

        if (!$request->filled('year')) {
            if ($request->filled('created_at')) {
                $merge['year'] = Carbon::parse($request->input('created_at'))->format('y');
            } else {
                $merge['year'] = date('y');
            }
        }

        if (!$request->filled('month')) {
            if ($request->filled('created_at')) {
                $merge['month'] = Carbon::parse($request->input('created_at'))->format('n');
            } else {
                $merge['month'] = date('n');
            }
        }

        if ((int)$request->input('amount') === 0) {
            $merge['paid'] = true;
        }

        $invoice = Invoice::create(array_merge($request->all(), $merge));

        if (!$invoice) {
            return 'Invoice was not saved.';
        }

        return $invoice;
    }

    public function storeManualInvoice(array $data)
    {
        $invoice = Invoice::create($data);

        if (!$invoice) {
            return 'Invoice was not saved.';
        }

        return $invoice;
    }

    /**
     * Save the invoice in PDF
     * Path: storage/app/invoices/<Ym>/
     *
     * @param $invoice
     */
    public function saveInvoicePdf($invoice)
    {
        if (!File::isDirectory($invoice->file_path)) {
            File::makeDirectory($invoice->file_path);
        }

        PDF::loadView('manifold.pdf.invoice.layout', ['invoice' => $invoice])
            ->save($invoice->file_path . $invoice->file);
    }
}
