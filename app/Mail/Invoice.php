<?php

namespace App\Mail;

// use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Invoice as InvoiceModel;
use Illuminate\Support\Facades\Storage;

/**
 * Class Invoice
 *
 * IMPORTANT: to make email to queue, we need the following:
 * - implements ShouldQueue (class)
 * - use Queueable (beginning of class)
 *
 * @package App\Mail
 */
class Invoice extends Mailable
{
    use SerializesModels;

    /**
     * @var InvoiceModel
     */
    public $invoice;

    public $images = [];

    /**
     * Create a new message instance.
     *
     * @param InvoiceModel $invoice
     */
    public function __construct(InvoiceModel $invoice)
    {
        $this->invoice = $invoice;

        foreach (Storage::disk('local')->files('email-images') as $file) {
            $fileName = basename($file);
            $this->images[substr(basename($fileName), 0, strrpos($fileName, '.'))] =
                storage_path('app/' . $file);
        }

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject(
                sprintf(
                    config('cubopark.email.subject.invoice'),
                    $this->invoice->number,
                    $this->invoice->tenant->units_string
                )
            )
            ->view('cubopark.email.accounts')
            ->attach($this->invoice->file_path . $this->invoice->file, [
                'as' => $this->invoice->file,
                'mime' => 'application/pdf',
            ]);
    }
}
