<?php

namespace App\Mail;

// use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Tenant;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

/**
 * Class Statement
 *
 * IMPORTANT: to make email to queue, we need the following:
 * - implements ShouldQueue (class)
 * - use Queueable (beginning of class)
 *
 * @package App\Mail
 */
class Statement extends Mailable
{
    use SerializesModels;

    public $statement;
    public $tenant;
    public $images = [];

    /**
     * Create a new message instance.
     *
     * @param $statement
     * @param $tenant
     */
    public function __construct(array $statement, Tenant $tenant)
    {
        $this->statement = $statement;
        $this->tenant = $tenant;

        foreach (Storage::disk('local')->files('email-images') as $file) {
            $fileName = basename($file);
            $this->images[substr(basename($fileName), 0, strrpos($fileName, '.'))] =
                storage_path('app/' . $file);
        }

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this
            ->subject(
                sprintf(
                    config('cubopark.email.subject.statement'),
                    $this->tenant->units_string
                )
            )
            ->view('cubopark.email.statement')
            ->attach($this->statement['path'] . $this->statement['file'], [
                'as' => $this->statement['file'],
                'mime' => 'application/pdf'
            ]);

        foreach ($this->statement['invoices'] as $invoice) {
            $this->attach($invoice->file_path . $invoice->file, [
                'as' => $invoice->file,
                'mime' => 'application/pdf',
            ]);
        }

        return $this;
    }
}
