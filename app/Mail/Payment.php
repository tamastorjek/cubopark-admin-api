<?php

namespace App\Mail;

// use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Tenant;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Payment as PaymentModel;
use Illuminate\Support\Facades\Storage;

/**
 * Class Invoice
 *
 * IMPORTANT: to make email to queue, we need the following:
 * - implements ShouldQueue (class)
 * - use Queueable (beginning of class)
 *
 * @package App\Mail
 */
class Payment extends Mailable
{
    use SerializesModels;

    /**
     * @var PaymentModel
     */
    public $payment;

    public $tenant;
    public $invoiceNumbers = [];
    public $images = [];

    /**
     * Create a new message instance.
     *
     * @param PaymentModel $payment
     * @param Tenant $tenant
     */
    public function __construct(PaymentModel $payment, Tenant $tenant)
    {
        $this->payment = $payment;
        $this->tenant = $tenant;

        foreach ($payment->invoices as $invoice) {
            $this->invoiceNumbers[] = $invoice->number;
        }

        $this->invoiceNumbers = implode('</b>, <b>', $this->invoiceNumbers);

        foreach (Storage::disk('local')->files('email-images') as $file) {
            $fileName = basename($file);
            $this->images[substr(basename($fileName), 0, strrpos($fileName, '.'))] =
                storage_path('app/' . $file);
        }

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this
            ->subject(
                sprintf(
                    config('cubopark.email.subject.payment'),
                    $this->payment->number,
                    $this->tenant->units_string
                )
            )
            ->view('cubopark.email.payment')
            ->attach($this->payment->pdf_file_path . $this->payment->pdf_file, [
                'as' => $this->payment->pdf_file,
                'mime' => 'application/pdf',
            ]);

        foreach ($this->payment->invoices as $invoice) {
            $this->attach($invoice->file_path . $invoice->file, [
                'as' => $invoice->file,
                'mime' => 'application/pdf',
            ]);
        }

        return $this;
    }
}
