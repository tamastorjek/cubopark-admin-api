<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;
use Nexmo\Client\Credentials\Basic;
use Nexmo\Client;
use Exception;

class Sms
{
    private $credentials;
    private $client;
    private $maxLength = 152; // deduct 8 characters for possible "RM 0.00 "

    public function __construct()
    {
        $this->credentials = new Basic(
            env('NEXMO_API_KEY', ''),
            env('NEXMO_API_SECRET', '')
        );

        $this->client = new Client($this->credentials);
    }

    /**
     * @param $to
     * @param $message
     * @return bool
     */
    public function send($to, $message): bool
    {
        if (strlen($message) > $this->maxLength) {
            return false;
        }

        try {
            $sms = $this->client->message()->send([
                'to' => $to,
                'from' => env('NEXMO_FROM', 'CUBOPARK'),
                'text' => $message
            ]);

            $response = $sms->getResponseData();

            if ((string)$response['messages'][0]['status'] === '0') {
                $this->sendCC($message);
                return true;
            }

            Log::error('SMS failed with status: ' . $response['messages'][0]['status']);
            return false;
        } catch (Exception $e) {
            Log::error('SMS was not sent. Error: ' . $e->getMessage());
            return false;
        }
    }

    private function sendCC($message) {
        try {
            $sms = $this->client->message()->send([
                'to' => config('cubopark.sms.cc'),
                'from' => env('NEXMO_FROM', 'CUBOPARK'),
                'text' => $message
            ]);

            $response = $sms->getResponseData();

            if ((string)$response['messages'][0]['status'] === '0') {
                return;
            }

            Log::error('SMS failed with status: ' . $response['messages'][0]['status']);
        } catch (Exception $e) {
            Log::error('SMS was not sent. Error: ' . $e->getMessage());
        }
    }
}
