<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'phone',
        'email',
        'primary',
        'notes'
    ];

    protected $hidden = [
        'deleted_at'
    ];

    public function tenant()
    {
        return $this->belongsTo('App\Models\Tenant');
    }
}
