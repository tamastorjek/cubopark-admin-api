<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    public function tenant()
    {
        return $this->belongsTo('App\Models\Tenant');
    }
}
