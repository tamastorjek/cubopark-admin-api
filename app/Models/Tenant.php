<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;

/**
 * Class Tenant
 * @package App\Models
 */
class Tenant extends Model
{
    use SoftDeletes;
    use SoftCascadeTrait;

    /**
     * Soft cascade relations
     *
     * @var array
     */
    protected $softCascade = [
        'contacts',
        'documents',
        'photos'
    ];

    protected $fillable = [
        'name',
        'reg_number',
        'address',
        'shop_name',
        'rental',
        'description',
        'business_type',
        'tenancy_length',
        'tenancy_start',
        'notes'
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
        'deleted_at'
    ];

    protected $appends = ['units_string', 'unit_ids', 'label', 'flags'];

    public function units()
    {
        return $this->belongsToMany('App\Models\Unit');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice')->orderBy('created_at');
    }

    public function readings()
    {
        return $this->hasMany('App\Models\MeterReading');
    }

    public function reports()
    {
        return $this->hasMany('App\Models\Report');
    }

    public function getPaymentsAttribute(): array
    {
        $payments = [];

        foreach ($this->invoices as $invoice) {
            $invoicePayments = $invoice->payments;

            foreach ($invoicePayments as $ip) {
                if (isset($payments[$ip->id])) {
                    continue;
                }

                $ip->invoice_id = $invoice->id;
                $payments[$ip->id] = $ip;
            }
        }

        usort(
            $payments,
            function ($a, $b) {
                return strcmp($a['created_at'], $b['created_at']);
            }
        );

        return $payments;
    }

    public function getBalanceAttribute(): array
    {
        $balance = [
            'invoiced' => 0,
            'paid' => 0,
            'current' => 0
        ];

        foreach ($this->invoices as $invoice) {
            $balance['invoiced'] += $invoice->amount;
            $balance['paid'] += $invoice->paid_amount;
        }

        $balance['current'] = $balance['paid'] - $balance['invoiced'];

        return $balance;
    }

    /**
     * Get all unit ids
     *
     * @return array
     */
    public function getUnitIdsAttribute(): array
    {
        $units = [];

        foreach ($this->units as $unit) {
            $units[] = $unit->id;
        }

        return $units;
    }

    /**
     * Build units string
     *
     * @return string
     */
    public function getUnitsStringAttribute(): string
    {
        $units = [];

        foreach ($this->units as $unit) {
            if (!isset($units[$unit->block])) {
                $units[$unit->block] = [];
            }

            $units[$unit->block][] = $unit->number;
        }

        foreach ($units as $block => $unit) {
            sort($unit);
            $units[$block] = trim($block . '-' . implode('/', $unit), '-');
        }

        return implode(', ', $units);
    }

    public function getLabelAttribute(): string
    {
        return $this->units_string . ' ・ ' . $this->shop_name;
    }

    public function getFlagsAttribute($strict = null): array
    {
        $flags = [];

        if (empty($this->tenancy_start)) {
            $flags['no_tenancy_start'] = true;
        }

        if ($strict !== false
            && !preg_match('/\d/', $this->address)
            || preg_match('/\?\?\?|to-change/i', $this->name)
            || !preg_match('/\d+/', $this->reg_number)
        ) {
            $flags['missing_business_info'] = true;
        }

        return $flags;
    }

    public function isInvoicable(): bool
    {
        // tenant info
        if ($this->getFlagsAttribute(false)) {
            return false;
        }

        // contacts (do not generate invoice if there's no one to send it to
        if (empty($this->contacts)) {
            return false;
        }

        // get the primary contact (or the first one if there's no primary)
        foreach ($this->contacts as $c) {
            if ($c->primary && !empty($c->email)) {
                return true;
            }
        }

        return false;
    }
}
