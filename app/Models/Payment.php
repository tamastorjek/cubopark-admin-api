<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'amount', 'type', 'reference', 'proof_file_ext', 'notes', 'year', 'month', 'created_at'
    ];

    protected $hidden = [
        'updated_at', 'deleted_at'
    ];

    protected $appends = [
        'amount_string', 'number', 'file', 'date', 'pdf_file'
    ];

    public function invoices()
    {
        return $this->belongsToMany('App\Models\Invoice');
    }

    public function getAmountStringAttribute(): string
    {
        return number_format(toFloat($this->amount), 2);
    }

    public function getNumberAttribute(): string
    {
        return config('payment.receipt_prefix')
            . $this->year . $this->getMonthString() . '-'
            . str_pad((string)$this->id, 8, '0', STR_PAD_LEFT);
    }

    public function getFileAttribute(): string
    {
        if (!$this->proof_file_ext) {
            return '';
        }

        return str_replace(['/', '.'], '-', $this->number) . '.' . $this->proof_file_ext;
    }

    public function getPdfFileAttribute(): string
    {
        return str_replace(['/', '.'], '-', $this->number) . '.pdf';
    }

    public function getDateAttribute(): string
    {
        return Carbon::parse($this->created_at)->format('d F Y');
    }

    public function getInvoiceNumbersAttribute(): array
    {
        $invoices = [];

        foreach ($this->invoices as $invoice) {
            $invoices[] = $invoice->number;
        }

        return $invoices;
    }

    public function getPdfFilePathAttribute(): string
    {
        return storage_path(
            implode(
                DIRECTORY_SEPARATOR,
                ['app', 'payments', '20' . $this->year . '-' . $this->getMonthString()]
            ) . DIRECTORY_SEPARATOR
        );
    }

    private function getMonthString()
    {
        return str_pad((string)$this->month, 2, 0, STR_PAD_LEFT);
    }
}
