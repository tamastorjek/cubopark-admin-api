<?php

namespace App\Models;

class MailRecipient
{
    public $name;
    public $email;

    public function __construct(string $name, string $email)
    {
        $this->name = $name;
        $this->email = $email;
    }
}
