<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $hidden = ['deleted_at'];

    protected $appends = ['string'];

    public function meters()
    {
        return $this->hasMany('App\Models\Meter');
    }

    public function tenants()
    {
        $this->belongsToMany('App\Models\Tenant');
    }

    public function getStringAttribute(): string
    {
        return $this->block . (empty($this->number) ? '' : '-' . $this->number);
    }
}
