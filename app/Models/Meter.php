<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meter extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $fillable = ['id', 'unit_id', 'type', 'last_reading', 'created_at', 'updated_at'];

    protected $keyType = 'string';

    protected $hidden = [
        'created_at',
        'deleted_at'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');
    }
}
