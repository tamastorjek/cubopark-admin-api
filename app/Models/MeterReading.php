<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeterReading extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'tenant_id',
        'meter_id',
        'reading',
        'type',
        'proof_file_ext',
        'notes',
        'created_at'
    ];

    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    protected $appends = ['file'];

    public function tenant()
    {
        return $this->belongsTo('App\Models\Tenant');
    }

    public function meter()
    {
        return $this->belongsTo('App\Models\Meter');
    }

    public function getFileAttribute(): string
    {
        if (!$this->proof_file_ext) {
            return '';
        }

        return Carbon::parse($this->created_at)->format('Ymd-') . $this->meter_id . '__'
            . str_replace('/', '-', $this->tenant->units_string)
            . '.' . $this->proof_file_ext;
    }
}
