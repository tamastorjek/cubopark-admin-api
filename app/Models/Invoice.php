<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Invoice extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'tenant_id',
        'type',
        'amount',
        'items',
        'paid',
        'paid_amount',
        'year',
        'month',
        'storno',
        'created_at'
    ];

    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    /**
     * Cast items field to JSON
     *
     * @var array
     */
    protected $casts = [
        'items' => 'array'
    ];

    protected $appends = ['amount_string', 'date', 'due', 'number', 'file', 'balance'];

    public function tenant()
    {
        return $this->belongsTo('App\Models\Tenant');
    }

    public function payments()
    {
        return $this->belongsToMany('App\Models\Payment')->orderBy('created_at');
    }

    public function getAmountStringAttribute(): string
    {
        return number_format(toFloat($this->amount), 2);
    }

    public function getPaidAmountStringAttribute(): string
    {
        return number_format(toFloat($this->paid_amount), 2);
    }

    public function getDateAttribute(): string
    {
        return Carbon::parse($this->created_at)->format('d F Y');
    }

    public function getNumberAttribute(): string
    {
        return config('payment.invoice_prefix')
            . $this->type . '/'
            . $this->year . $this->getMonthString() . '-'
            . str_pad((string)$this->id, 8, '0', STR_PAD_LEFT);
    }

    public function getDueDate($format = 'd F Y')
    {
        return Carbon::parse($this->created_at)->addWeeks()->format($format);
    }

    public function getDueAttribute(): string
    {
        return $this->getDueDate();
    }

    public function getItemsAttribute($items): array
    {
        $items = (array)json_decode($items);

        foreach ($items as &$item) {
            $item->amount_string = number_format(toFloat($item->amount), 2);
        }

        return $items;
    }

    public function getFileAttribute(): string
    {
        return str_replace(['/', '.'], '-', $this->number) . '.pdf';
    }

    public function getFilePathAttribute(): string
    {
        return storage_path(
            implode(
                DIRECTORY_SEPARATOR,
                ['app', 'invoices', '20' . $this->year . '-' . $this->getMonthString()]
            ) . DIRECTORY_SEPARATOR
        );
    }

    public function getBalanceAttribute(): int
    {
        return $this->amount - $this->paid_amount;
    }

    public function getBalanceStringAttribute(): string
    {
        return number_format(toFloat($this->balance), 2);
    }

    private function getMonthString()
    {
        return str_pad((string)$this->month, 2, 0, STR_PAD_LEFT);
    }
}
