<?php

function toFloat(int $val): float
{
    return $val / 100;
}

function toInt($val): int
{
    if (!$val) {
        return 0;
    }

    return round($val * 100);
}

function timestampToDate(string $ts): string
{
    if (empty($ts)) {
        return null;
    }

    return explode(' ', $ts)[0];
}
