<?php

namespace App\Http\Resources;

use App\Models\Tenant;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TenantCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Tenant $tenant) {
            return (new TenantResource($tenant));
        });

        $tenants = parent::toArray($request);

        // sort tenants by units_string
        usort(
            $tenants,
            function ($a, $b) {
                return strcmp($a['units_string'], $b['units_string']);
            }
        );

        // get invoice count for all active tenants
        $invoiceCount = $this->getInvoiceCount(array_column($tenants, 'id'));

        // get tenants with email contacts
        $contacts = $this->getMailableTenants();

        foreach ($tenants as &$tenant) {
            $tenant['flags']['rent_invoices'] = isset($invoiceCount[$tenant['id']])
                ? $invoiceCount[$tenant['id']]->invoices
                : 0;

            $tenant['flags']['months'] = Carbon::parse($tenant['tenancy_start'])->diffInMonths();

            if (!in_array($tenant['id'], $contacts)) {
                $tenant['flags']['no_email'] = true;
            }
        }

        return $tenants;
    }

    /**
     * Get invoice count for all active tenants
     *
     * @param $activeTenants
     * @return array
     */
    private function getInvoiceCount(array $activeTenants): array
    {
        $invoiceCount = DB::table('invoices')
            ->select(DB::raw('tenant_id, count(*) as invoices'))
            ->where('type', 'RT')
            ->whereNull('storno')
            ->whereNull('deleted_at')
            ->whereIn('tenant_id', $activeTenants)
            ->groupBy('tenant_id')
            ->get()
            ->toArray();

        return array_combine(array_column($invoiceCount, 'tenant_id'), $invoiceCount);
    }

    /**
     * Get tenants with mailable contacts
     *
     * @return array
     */
    private function getMailableTenants(): array
    {
        return DB::table('contacts')
            ->select('tenant_id')
            ->where('primary', 1)
            ->whereNotNull('email')
            ->groupBy('tenant_id')
            ->get()
            ->pluck('tenant_id')
            ->toArray();
    }
}
