<?php

namespace App\Http\Resources;

use App\Models\Meter;
use App\Models\Tenant;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MeterCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        $meters = parent::toArray($request);
        $tenants = Tenant::all();

        foreach ($meters as &$meter) {
            foreach ($tenants as $tenant) {
                if (in_array($meter['unit_id'], $tenant->unit_ids)) {
                    $meter['tenant_id'] = $tenant->id;
                    continue 2;
                }
            }

            $meter['tenant_id'] = null;
        }

        return $meters;
    }
}
