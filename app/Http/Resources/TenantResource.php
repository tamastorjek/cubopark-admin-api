<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TenantResource extends JsonResource
{
    /**
     * If true, show all relations for the tenant.
     *
     * @var bool
     */
    protected $showDetails = false;

    private $tenantRelations = [
        'units',
        'contacts',
        'documents',
        'photos',
        'invoices',
        // 'payments',
        'reports'
    ];

    public $unitString;

    /**
     * Override parent constructor, so we can add details for show.
     *
     * @param $resource
     * @param bool $showDetails
     */
    public function __construct($resource, bool $showDetails = false)
    {
        $this->showDetails = $showDetails;

        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $model = parent::toArray($request);

        // we need the invoices and payments, to see how much the tenant owes
        $model['balance'] = $this->balance;

        if (!$this->showDetails) {
            // remove all relations
            foreach ($this->tenantRelations as $relation) {
                unset($model[$relation]);
            }

            return $model;
        }

        // add meter info to units
        foreach ($this->units as $unit) {
            $unit->meters;
        }

        // add all relations
        foreach ($this->tenantRelations as $relation) {
            $model[$relation] = $this->$relation;
        }

        // add payments to invoices
        foreach ($model['invoices'] as &$invoice) {
            $invoice->payments;
        }

        // add readings
        $model['readings'] = MeterReadingResource::collection($this->readings);

        return $model;
    }
}
