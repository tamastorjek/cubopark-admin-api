<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MeterReadingResource extends JsonResource
{
    /**
     * If true, show all relations for the tenant.
     *
     * @var bool
     */
    protected $showDetails = false;

    /**
     * Override parent constructor, so we can add details for show.
     *
     * @param $resource
     * @param bool $showDetails
     */
    public function __construct($resource, bool $showDetails = false)
    {
        $this->showDetails = $showDetails;

        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $model = parent::toArray($request);

        if (!$this->showDetails) {
            unset($model['tenant']);
        }

        return $model;
    }
}
