<?php

namespace App\Http\Controllers;

use App\Models\Meter;
use App\Models\Tenant;
use App\Models\Unit;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UnitsController extends JsonController
{
    /**
     * Entry method
     *
     * @param string $condition
     * @return JsonResponse
     */
    public function index(string $condition = 'all'): JsonResponse
    {
        switch ($condition) {
            case 'occupied':
                return $this->respondWithSuccess($this->getOccupied());

            case 'free':
                return $this->respondWithSuccess($this->getFree());

            default:
                return $this->respondWithSuccess(Unit::all());
        }
    }

    /**
     * Get occupied units
     *
     * @return array
     */
    public function getOccupied(): array
    {
        $units = [];
        $tenants = Tenant::all();
        $meters = Meter::all();
        $occupiedMeters = [];

        foreach ($meters as $meter) {
            $lastReadingDate = Carbon::parse($meter->updated_at);
            $meter->disabled = !($lastReadingDate->diffInDays(Carbon::now()) > 7);

            if (!isset($occupiedMeters[$meter->unit_id])) {
                $occupiedMeters[$meter->unit_id] = [];
            }

            $occupiedMeters[$meter->unit_id][] = $meter;
        }

        foreach ($tenants as $tenant) {
            $numbers = $tenant->units->pluck('number');
            $block = $tenant->units->first()->block;
            $unitIds = $tenant->units->pluck('id');
            $unitMeters = [];
            $disabled = true;

            foreach ($unitIds as $unitId) {
                if (isset($occupiedMeters[$unitId])) {
                    $unitMeters = array_merge($unitMeters, $occupiedMeters[$unitId]);

                    foreach ($unitMeters as $um) {
                        if (!$um->disabled) {
                            $disabled = false;
                            break;
                        }
                    }
                }
            }

            // sort meters by type & unit number
            usort(
                $unitMeters,
                function ($a, $b) {
                    $type = strcmp($a['type'], $b['type']);

                    return $type === 0 ? $a['unit_id'] - $b['unit_id'] : $type;
                }
            );

            $unitString = $numbers->implode('/');
            $unitString = empty($unitString) ? $block : $block . '-' . $unitString;

            $units[] = [
                'tenant_id' => $tenant->id,
                'id' => $unitIds,
                'block' => $block,
                'numbers' => $numbers,
                'string' => $unitString,
                'meters' => $unitMeters,
                'disabled' => $disabled,
                'label' => $tenant->label
            ];
        }

        usort(
            $units,
            function ($a, $b) {
                return strcmp($a['string'], $b['string']);
            }
        );

        return $units;
    }

    /**
     * Get free units
     *
     * @return Collection
     */
    public function getFree(): Collection
    {
        // get occupied unit IDs
        $occupied = DB::table('tenant_unit')
            // get active tenant IDs
            ->whereIn('tenant_id', Tenant::all()->pluck('id'))
            ->distinct()
            ->pluck('unit_id');

        // get free units based on occupied unit IDs
        return Unit::all()->whereNotIn('id', $occupied)->flatten();
    }
}
