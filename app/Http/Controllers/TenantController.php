<?php

namespace App\Http\Controllers;

use App\Http\Resources\TenantCollection;
use Illuminate\Http\Request;
use App\Models\Tenant;
use App\Http\Resources\TenantResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TenantController extends JsonController
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->respondWithSuccess(
            new TenantCollection(Tenant::all())
        );
    }

    /**
     * Get list of tenant names with unit numbers
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function autocomplete(Request $request)
    {
        $tenants = $this->index()->toArray($request);
        $list = [];

        foreach ($tenants as $tenant) {
            // shop name + unit string (will be searched with regex)
            $list[] = $tenant['shop_name'] . ' (' . $tenant['units_string'] . ')';
        }

        return $this->respondWithSuccess($list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'reg_number' => 'required|max:32|regex:/^[a-zA-Z0-9-]{6,32}$/i',
            'address' => 'required|string|max:255',
            'shop_name' => 'required|string|max:255',
            'rental' => 'required|integer|max:9999900',
            'business_type' => 'required',
            'tenancy_start' => 'date_format:Y-m-d|nullable',
            'tenancy_length' => 'required|integer|between:1,3',
            'notes' => 'string|max:255|nullable',
            'units' => 'array|min:1',
        ]);

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors());
        }

        // ignore id, even if present
        $tenant = Tenant::create($request->all());

        if (!$tenant) {
            return $this->respondWithError('Tenant was not saved.');
        }

        // save units
        $tenant->units()->attach($request['units']);

        // save primary contact
        // $tenant->contacts()->create($request['contact']);

        return $this->respondWithSuccess($tenant);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return TenantResource|JsonResponse
     */
    public function show($id)
    {
        $tenant = Tenant::find($id);

        if (empty($tenant)) {
            return $this->respondWithError('Cannot find tenant.');
        }

        return $this->respondWithSuccess(
            new TenantResource($tenant, true)
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'action' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors());
        }

        $action = 'update' . str_replace(' ', '',
                ucwords(str_replace('-', ' ', $request['action']))
            );

        if (!method_exists($this, $action)) {
            return $this->respondWithError('Invalid action.');
        }

        return $this->$action($request, $id);
    }

    private function updateInfo(Request $request, $id): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'reg_number' => 'required|max:32|regex:/^[a-zA-Z0-9-]{6,32}$/i',
            'address' => 'string|max:255|nullable',
            'shop_name' => 'required|string|max:255',
            'rental' => 'required|integer|max:9999900',
            'business_type' => 'required',
            'tenancy_start' => 'date_format:Y-m-d|nullable',
            'tenancy_length' => 'required|integer|between:1,3',
            'notes' => 'string|max:255|nullable',
            'units' => 'array|min:1',
        ]);

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors());
        }

        $tenant = Tenant::find($id);

        if (!$tenant) {
            return $this->respondWithError('Cannot find tenant.');
        }

        if (!$tenant->fill($request->all())->save()) {
            return $this->respondWithError('Tenant was not saved.');
        }

        $tenant->units()->sync($request['units']);

        return $this->respondWithSuccess($tenant);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        if (Tenant::destroy($id)) {
            return $this->respondWithSuccess($id);
        }

        return $this->respondWithError('Tenant was not deleted.');
    }

    public function massStore(Request $request)
    {
        $added = 0;

        foreach ($request->tenants as $t) {
            // ignore id, even if present
            $tenant = Tenant::create($t['tenant']);

            if (!$tenant) {
                return $this->respondWithError('Tenant was not saved: ' . $t['tenant']->name);
            }

            // save units
            $tenant->units()->attach($t['units']);

            // save primary contact
            $tenant->contacts()->create($t['contact']);

            ++$added;
        }

        return $this->respondWithSuccess('Added ' . $added . ' tenants.');
    }

    public function getTenantsWithInvoice($type = 'all')
    {
        $query = DB::table('invoices')
            ->select('tenant_id')
            ->whereNull('deleted_at');

        if ($type !== 'all') {
            $query->where('paid', $type === 'paid');
        }

        $tenantIds = $query
            ->distinct()
            ->get()
            ->pluck('tenant_id')
            ->toArray();

        return $this->respondWithSuccess(
            new TenantCollection(
                Tenant::find($tenantIds)
            )
        );
    }
}
