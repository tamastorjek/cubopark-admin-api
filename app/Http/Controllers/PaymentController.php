<?php

namespace App\Http\Controllers;

use App\Mail\Payment as PaymentMailTemplate;
use App\Models\Invoice;
use App\Models\MailRecipient;
use App\Models\Payment;
use App\Models\Sms;
use App\Models\Tenant;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Traits\PaymentStoreTrait;
use Illuminate\Support\Facades\Mail;
use js\tools\numbers2words\Speller as NumToWords;

class PaymentController extends JsonController
{
    use PaymentStoreTrait;

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->respondWithSuccess(Payment::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $payment = $this->storePayment($request);

        if ($payment instanceof Payment) {
            // send email
            $this->sendMail($payment);
            // send SMS
            // $this->sendSms($payment);

            return $this->respondWithSuccess($payment);
        }

        return $this->respondWithError($payment);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);

        if (empty($invoice)) {
            return $this->respondWithError('Cannot find invoice.');
        }

        return $this->respondWithSuccess($invoice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //
    }

    public function stream($id)
    {
        $payment = Payment::find($id);

        if (empty($payment)) {
            return $this->respondWithError('Cannot find payment.');
        }

        $payment->amount_words = str_replace(' euro', '',
            NumToWords::spellCurrency(
                $payment->amount,
                NumToWords::LANGUAGE_ENGLISH,
                NumToWords::CURRENCY_EURO,
                true,
                true
            )
        );

        return PDF::loadView(
            'manifold.payment.layout',
            [
                'payment' => $payment,
                'tenant' => Tenant::find($payment->invoices[0]->tenant_id)
            ]
        )->stream();
    }

    /**
     * @param $id
     * @param $page =1
     * @return JsonResponse
     */
    public function regenerate($id, $page = 1): JsonResponse
    {
        if ($id === 'all') {
            $limit = 20;
            $offset = ((int)$page - 1) * $limit;
            $payments = Payment::all()->slice($offset, $limit);

            if (empty($payments)) {
                return $this->respondWithSuccess('Done: ' . $offset . ' - ' . ($offset + $limit));
            }
        } else {
            $payments = [Payment::find($id)];
        }

        if (empty($payments)) {
            return $this->respondWithError('Cannot find payment.');
        }

        return $this->respondWithSuccess($this->rebuildPayments($payments));
    }

    public function regenerateByTenant($tenatId): JsonResponse
    {
        $tenant = Tenant::find($tenatId);

        if (empty($tenant)) {
            return $this->respondWithError('Tenant not found.');
        }

        if (empty($tenant->payments)) {
            return $this->respondWithError('Tenant has no payment.');
        }

        return $this->respondWithSuccess($this->rebuildPayments($tenant->payments));
    }

    /**
     * @param $payments
     * @return array
     */
    private function rebuildPayments($payments) : array
    {
        $updated = [
            'count' => count($payments),
            'files' => []
        ];

        foreach ($payments as $payment) {
            $this->savePaymentPdf($payment);
            $updated['files'][] = $payment->pdf_file_path . $payment->pdf_file;
        }

        return $updated;
    }

    private function getMailRecipients($contacts, $invoices): array
    {
        $response = [
            'status' => true,
            'recipients' => [],
            'bcc' => [],
            'message' => ''
        ];

        if (empty($contacts)) {
            $response['status'] = false;
            $response['message'] = 'No contacts for this tenant.';

            return $response;
        }

        // get the primary contact (or the first one if there's no primary)
        foreach ($contacts as $c) {
            if ($c->primary && !empty($c->email)) {
                $response['recipients'][] = $c;
            }
        }

        if (empty($response['recipients'])) {
            $response['status'] = false;
            $response['message'] = 'No mailable contact for this tenant.';

            return $response;
        }

        foreach (config('cubopark.email.bcc.primary') as $r) {
            $response['bcc'][] = new MailRecipient($r[0], $r[1]);
        }

        // add misc bcc/recipients
        foreach ($invoices as $invoice) {
            if ($invoice->type === 'FF') {
                foreach (config('cubopark.email.bcc.facility') as $r) {
                    $response['bcc'][] = new MailRecipient($r[0], $r[1]);
                }

                break;
            }
        }

        return $response;
    }

    private function sendMail(Payment $payment)
    {
        $invoices = &$payment->invoices;

        if (empty($invoices)) {
            return 'No invoices were found for this payment (' . $payment->id . ').';
        }

        $tenant = Tenant::find($invoices->first()->tenant_id);

        $mailData = $this->getMailRecipients($tenant->contacts, $invoices);

        if (!$mailData['status']) {
            return $mailData['message'];
        }

        try {
            Mail::to($mailData['recipients'])
                ->bcc($mailData['bcc'])
                ->send(new PaymentMailTemplate($payment, $tenant));

            // Log::info('Recipients: ' . implode(', ', $recipients));

            return $mailData['recipients'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function getSmsRecipients($contacts): array
    {
        if (empty($contacts)) {
            return [
                'status' => false,
                'message' => 'No contacts for this tenant.'
            ];
        }

        $response = [
            'status' => true,
            'recipients' => []
        ];

        // get the primary contact (or the first one if there's no primary)
        foreach ($contacts as $c) {
            if ($c->primary && !empty($c->phone) && !empty($c->email)) {
                $response['recipients'][] = $c;
            }
        }

        if (empty($response['recipients'])) {
            return [
                'status' => false,
                'message' => 'No contact with phone number & email for this tenant.'
            ];
        }

        return $response;
    }

    private function sendSms(Invoice $invoice)
    {
        $smsData = $this->getSmsRecipients($invoice->tenant->contacts);

        $sms = new Sms();
        $sentTo = [];

        foreach ($smsData['recipients'] as $recipient) {
            $sent = $sms->send(
                $recipient->phone,
                sprintf(
                    config('cubopark.sms.invoice'),
                    $invoice->tenant->units_string,
                    $invoice->amount_string,
                    $invoice->getDueDate('d/m/Y')
                )
            );

            if ($sent) {
                $sentTo[] = $recipient->name . ' (' . $recipient->phone . ')';
            }
        }

        return $sentTo;
    }

    public function sendForcedMail($id): JsonResponse
    {
        $payment = Payment::find($id);

        if (!$payment) {
            return $this->respondWithError('No payment was found with this id.');
        }

        $mailSent = $this->sendMail($payment);

        return is_array($mailSent)
            ? $this->respondWithSuccess($mailSent)
            : $this->respondWithError($mailSent);
    }

    public function sendForcedMailTenant($id): JsonResponse
    {
        $tenant = Tenant::find($id);

        if (!$tenant) {
            return $this->respondWithError('No tenant was found with this id.');
        }

        $response = [
            'sent' => [],
            'error' => []
        ];

        return $this->respondWithSuccess($tenant->payments);

        foreach ($tenant->payments->distinct() as $payment) {
            $mailSent = $this->sendMail($payment);

            $response[is_array($mailSent) ? 'sent' : 'error'][] = $payment->number;
        }

        return $this->respondWithSuccess($response);
    }
}
