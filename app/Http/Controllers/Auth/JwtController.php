<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\JsonController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Token;

/**
 * Class JwtController
 *
 * @package App\Http\Controllers
 */
class JwtController extends JsonController
{
    /**
     * Creates a new user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request['user'], [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors(), 400);
        }

        $user = User::create([
            'name'    => $request['user']['name'],
            'email'    => $request['user']['email'],
            'password' => Hash::make($request['user']['password']),
        ]);

        if (!$user) {
            return $this->respondWithError('User was not created.', 400);
        }

        $token = $user->createToken(env('JWT_TOKEN_APP_NAME'));

        return $this->respondWithToken($token);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request['user'], [
            'email' => 'required|exists:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors(), 400);
        }

        if (!Auth::guard('web')->attempt($request['user'])) {
            return $this->respondWithError('Unauthorized', 401);
        }

        $user = Auth::guard('web')->user();

        /**
         * @todo check if user already has a token
         */

        $token = $user->createToken(env('JWT_TOKEN_APP_NAME'));

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function user()
    {
        return response()->json(Auth::user());
    }

    /**
     * Log the user out (Invalidates the token).
     *
     * @return JsonResponse
     */
    public function logout()
    {
        Auth::logout();

        return $this->respondWithSuccess();
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token->accessToken,
            'token_type' => 'bearer',
            'expires_in' => $token->token['expires_at']
        ]);
    }
}
