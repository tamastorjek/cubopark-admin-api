<?php

namespace App\Http\Controllers;

use Exception;

class ConfigController extends JsonController
{
    private $configs = [
        'payment'
    ];

    public function index() {
        $data = [];

        foreach ($this->configs as $key) {
            $data[$key] = config($key);
        }

        return $this->respondWithSuccess($data);
    }
}
