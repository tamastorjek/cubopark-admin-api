<?php

namespace App\Http\Controllers;

use App\Http\Resources\TenantResource;
use App\Models\Sms;
use Exception;
use App\Models\Invoice;
use App\Models\MailRecipient;
use App\Models\Payment;
use App\Models\Tenant;
use App\Traits\InvoiceStoreTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Traits\PaymentStoreTrait;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\Invoice as InvoiceMailTemplate;
use App\Mail\Statement as StatementMailTemplate;
use App\Mail\FullStatement as FullStatementMailTemplate;

class InvoiceController extends JsonController
{
    use PaymentStoreTrait;
    use InvoiceStoreTrait;

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->respondWithSuccess(Invoice::all());
    }

    public function getByTenant($tenantId, $type, $withPayments = false)
    {
        if (!is_bool($withPayments)) {
            $withPayments = (bool)$withPayments;
        }

        $where = [['tenant_id', $tenantId]];

        if ($type !== 'all') {
            $where[] = ['paid', $type === 'paid'];
        }

        $invoices = Invoice::where($where)
            ->orderBy('type')
            ->orderBy('created_at')
            ->get();

        if (!$invoices) {
            return $this->respondWithError('No invoices were found.');
        }

        if ($withPayments) {
            foreach ($invoices as &$invoice) {
                $invoice->payments;
            }
        }

        return $this->respondWithSuccess($invoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $invoice = $this->storeInvoice($request);

        if (!($invoice instanceof Invoice)) {
            return $this->respondWithError($invoice);
        }

        // generate and save invoice
        $this->saveInvoicePdf($invoice);

        if (!$request->filled('no_communication')) {
            // send email
            $this->sendMail($invoice);
            // send sms
            $this->sendInvoiceSms($invoice);
        }

        // create a receipt right away
        if ($request->filled('payment.store')) {
            $payment = $this->storePayment($request, [$invoice->id]);

            if (!($payment instanceof Payment)) {
                return $this->respondWithError($payment);
            }

            // update paid flag if payment.amount = invoice.amount
            if ($invoice->amount === $payment->amount) {
                $invoice->paid = true;
                $invoice->save();
            }
        }

        $invoice->payments;

        return $this->respondWithSuccess($invoice);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);

        if (empty($invoice)) {
            return $this->respondWithError('Cannot find invoice.');
        }

        $invoice->payments;

        return $this->respondWithSuccess($invoice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @param $page =1
     * @return JsonResponse
     */
    public function regenerate($id, $page = 1): JsonResponse
    {
        if ($id === 'all') {
            $limit = 20;
            $offset = ((int)$page - 1) * $limit;
            $invoices = Invoice::all()->slice($offset, $limit);

            if (empty($invoices)) {
                return $this->respondWithSuccess('Done: ' . $offset . ' - ' . ($offset + $limit));
            }
        } else {
            $invoices = [Invoice::find($id)];
        }

        if (empty($invoices)) {
            return $this->respondWithError('Cannot find invoice.');
        }

        return $this->respondWithSuccess($this->rebuildInvoices($invoices));
    }

    public function regenerateByTenant($tenatId): JsonResponse
    {
        $tenant = Tenant::find($tenatId);

        if (empty($tenant)) {
            return $this->respondWithError('Tenant not found.');
        }

        if (empty($tenant->invoices)) {
            return $this->respondWithError('Tenant has no invoice.');
        }

        return $this->respondWithSuccess($this->rebuildInvoices($tenant->invoices));
    }

    /**
     * @param $invoices
     * @return array
     */
    private function rebuildInvoices($invoices) : array
    {
        $updated = [
            'count' => count($invoices),
            'files' => []
        ];

        foreach ($invoices as $invoice) {
            if (!is_object($invoice->tenant)) {
                Log::error($invoice);
                continue;
            }

            $this->saveInvoicePdf($invoice);
            $updated['files'][] = $invoice->file_path . $invoice->file;
        }

        return $updated;
    }

    /**
     * Stream
     *
     * @param $id
     * @return JsonResponse
     */
    public function stream($id)
    {
        $invoice = Invoice::find($id);

        if (empty($invoice)) {
            return $this->respondWithError('Cannot find invoice.');
        }

        return PDF::loadView('manifold.pdf.invoice.layout', ['invoice' => $invoice])->stream();
    }

    public function updateDetailsAmountToInt()
    {
        $invoices = Invoice::all();

        foreach ($invoices as $invoice) {
            $invoice->items = $invoice->items['items'];
            $invoice->save();
        }
    }

    private function getMailRecipients($contacts): array
    {
        $response = [
            'status' => true,
            'recipients' => [],
            'bcc' => [],
            'message' => ''
        ];

        if (empty($contacts)) {
            $response['status'] = false;
            $response['message'] = 'No contacts for this tenant.';

            return $response;
        }

        // get the primary contact (or the first one if there's no primary)
        foreach ($contacts as $c) {
            if ($c->primary && !empty($c->email)) {
                $response['recipients'][] = $c;
            }
        }

        if (empty($response['recipients'])) {
            $response['status'] = false;
            $response['message'] = 'No mailable contact for this tenant.';

            return $response;
        }

        foreach (config('cubopark.email.bcc.primary') as $r) {
            $response['bcc'][] = new MailRecipient($r[0], $r[1]);
        }

        return $response;
    }

    private function sendMail(Invoice $invoice)
    {
        $mailData = $this->getMailRecipients($invoice->tenant->contacts);

        if (!$mailData['status']) {
            return $mailData['message'];
        }

        try {
            Mail::to($mailData['recipients'])
                ->bcc($mailData['bcc'])
                ->send(new InvoiceMailTemplate($invoice));

            // Log::info('Recipients: ' . implode(', ', $recipients));

            return $mailData['recipients'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function getSmsRecipients($contacts): array
    {
        if (empty($contacts)) {
            return [
                'status' => false,
                'message' => 'No contacts for this tenant.'
            ];
        }

        $response = [
            'status' => true,
            'recipients' => []
        ];

        // get the primary contact (or the first one if there's no primary)
        foreach ($contacts as $c) {
            if ($c->primary && !empty($c->phone) && !empty($c->email)) {
                $response['recipients'][] = $c;
            }
        }

        if (empty($response['recipients'])) {
            return [
                'status' => false,
                'message' => 'No contact with phone number & email for this tenant.'
            ];
        }

        return $response;
    }

    private function sendSms(Tenant $tenant, string $messageText) {
        $smsData = $this->getSmsRecipients($tenant->contacts);

        $sms = new Sms();
        $sentTo = [];

        foreach ($smsData['recipients'] as $recipient) {
            $sent = $sms->send(
                $recipient->phone,
                $messageText
            );

            if ($sent) {
                $sentTo[] = $recipient->name . ' (' . $recipient->phone . ')';
            } else {
                Log::error(
                    'SMS failed to: ' . $tenant->units_string .
                    ' (' . $recipient->name . ' / ' . $recipient->phone . ')'
                );
            }
        }

        return $sentTo;
    }

    private function sendInvoiceSms(Invoice $invoice)
    {
        return $this->sendSms(
            $invoice->tenant,
            sprintf(
                config('cubopark.sms.invoice'),
                $invoice->tenant->units_string,
                $invoice->amount_string,
                $invoice->getDueDate('d/m/Y')
            )
        );
    }

    private function sendStatementSms($statement)
    {
        return $this->sendSms(
            $statement['tenant'],
            sprintf(
                config('cubopark.sms.statement'),
                $statement['tenant']->units_string,
                // $recipient->email,
                $statement['total'],
                $statement['due']
            )
        );
    }

    private function createStatement(
        Tenant $tenant,
        $paid = 'all',
        $invoiceType = 'all',
        $returnInvoices = false): array
    {
        $where = [['tenant_id', $tenant->id]];

        if ($paid !== 'all') {
            $where[] = ['paid', $paid === 'paid'];
        }

        if ($invoiceType !== 'all') {
            $where[] = ['type', strtoupper($invoiceType)];
        }

        $invoices = Invoice::where($where)
            ->orderBy('created_at')
            ->get();

        if (empty($invoices)) {
            return [
                'status' => false,
                'message' => 'No unpaid invoices were found.'
            ];
        }

        $path = storage_path(
            implode(
                DIRECTORY_SEPARATOR,
                [
                    'app',
                    ($paid !== 'unpaid' ? 'full-' : '') . 'statements',
                    date('Y-m')
                ]
            )
        );

        $filename = 'Statement '
            . str_pad((string)$tenant->id, 4, '0', STR_PAD_LEFT)
            . '-' . date('Ymd') . '.pdf';

        $response = [
            'status' => true,
            'path' => $path . DIRECTORY_SEPARATOR,
            'file' => $filename,
            'due' => Carbon::now()->addWeeks(1)->format('d/m/Y')
        ];

        if ($returnInvoices) {
            $response['invoices'] = $invoices;
        }

        $total = 0;

        foreach ($invoices as $invoice) {
            $total = $total + $invoice->amount - $invoice->paid_amount;
        }

        $response['total'] = number_format(toFloat($total), 2);

        if (!File::isDirectory($path)) {
            try {
                File::makeDirectory($path, 0755, true);
            } catch (Exception $e) {
                return [
                    'status' => false,
                    'message' => $e->getMessage() . ' (' . $path . ')'
                ];
            }
        }

        // check if statement from same day exists
        if (File::exists($path . DIRECTORY_SEPARATOR . $filename)) {
            return $response;
        }

        PDF::loadView(
            $paid === 'unpaid' ? 'manifold.pdf.statement.layout' : 'manifold.pdf.statement_full.layout',
            [
                'invoices' => $invoices,
                'tenant' => $tenant,
                'date' => date('d F Y'),
                'total' => $response['total'],
                'due' => Carbon::now()->addWeeks(1)->format('d F Y')
            ]
        )
            ->save($path . DIRECTORY_SEPARATOR . $filename);

        return $response;
    }

    public function statement($tenantId, $paid = 'unpaid', $invoiceType = 'all'): JsonResponse
    {
        $statement = $this->createStatement(Tenant::find($tenantId), $paid, $invoiceType);

        if (!$statement['status']) {
            return $this->respondWithError($statement['message']);
        }

        return $this->respondWithSuccess($statement);
    }

    /**
     * Get list of missing rental invoice dates
     *
     * @param null $tenantId
     * @return JsonResponse
     * @todo handle single tenant ID
     *
     */
    public function getMissingRentalInvoices($tenantId = null): JsonResponse
    {
        return $this->respondWithSuccess($this->buildMissingInvoices($tenantId));
    }

    private function buildMissingInvoices(int $tenantId = null): array
    {
        /**
         * @var array $tenancyDates
         * @var array $missingInvoices
         * @var array $invoices
         */
        extract($this->prepareMissingRentalInvoiceData($tenantId));

        unset($tenantId);

        foreach ($tenancyDates as $tenantId => $tenancyStart) {
            if (empty($tenancyStart)) {
                continue;
            }

            $this->prefillMissingInvoices($tenancyStart, $missingInvoices[$tenantId]);

            foreach (['day', 'month', ''] as $strict) {
                $this->matchInvoiceDate(
                    $tenancyStart,
                    $invoices[$tenantId],
                    $missingInvoices[$tenantId],
                    $strict
                );
            }
        }

        return $missingInvoices;
    }

    private function prepareMissingRentalInvoiceData(int $tenantId = null): array
    {
        $data = [];

        $tenants = DB::table('tenants')
            ->select(['id', 'tenancy_start'])
            ->whereNull('deleted_at');

        if ($tenantId) {
            $tenants->where('id', $tenantId);
        }

        $tenants = $tenants->get()->toArray();

        $tenantIds = array_column($tenants, 'id');
        $data['tenancyDates'] = array_combine(
            $tenantIds,
            array_column($tenants, 'tenancy_start')
        );

        $invoiceListQuery = DB::table('invoices')
            ->select(['tenant_id', 'created_at'])
            ->whereNull('storno')
            ->where('type', 'RT')
            ->whereIn('tenant_id', $tenantIds);

        $invoiceListQuery->orderBy('tenant_id');
        $invoiceListQuery->orderBy('created_at');

        $invoiceList = $invoiceListQuery->get()->toArray();

        // group invoices by tenant_id
        $data['invoices'] = array_fill_keys($tenantIds, []);

        foreach ($invoiceList as $item) {
            $data['invoices'][$item->tenant_id][] = timestampToDate($item->created_at);
        }

        // loop through tenants and add missing invoices
        $data['missingInvoices'] = array_fill_keys($tenantIds, []);

        return $data;
    }

    private function prefillMissingInvoices($tenancyStart, &$missingInvoices)
    {
        $now = Carbon::now();
        $startDate = Carbon::parse($tenancyStart)->addMonth();

        while ($startDate->isBefore($now)) {
            $key = $startDate->format('Y-m-d');
            $period = $startDate->format('d/m/Y');
            $startDate->addMonth();
            $period .= ' - ' . $startDate->format('d/m/Y');
            $missingInvoices[$key] = $period;
        }
    }

    /**
     * Match invoices to rent period
     *
     * @param string $tenancyStart
     * @param $invoices
     * @param $missingInvoices
     * @param string $strict 'day'|'month'\'' -- if empty, just match by sequence
     */
    private function matchInvoiceDate(
        $tenancyStart,
        &$invoices,
        &$missingInvoices,
        $strict = 'day'
    ): void
    {
        if (empty($invoices)) {
            return;
        }

        $now = Carbon::now();
        $startDate = Carbon::parse($tenancyStart);
        $startDate->addMonth();

        while ($startDate->isBefore($now)) {
            $match = false;
            $invoiceIndex = false;
            $key = $startDate->format('Y-m-d');

            switch ($strict) {
                case 'day':
                    $invoiceIndex = array_search($key, $invoices);
                    $match = $invoiceIndex !== false;
                    break;

                case 'month':
                    $currentDate = $startDate->format('Y-m');

                    foreach ($invoices as $invoiceIndex => $invoice) {
                        if (strpos($invoice, $currentDate) === 0) {
                            $match = true;
                            break 2;
                        }
                    }

                    break;

                default:
                    $match = empty($missingInvoices[$key]) && !empty($invoices);
                    $invoiceIndex = $match ? 0 : false;

                    break;
            }

            if ($match && $invoiceIndex !== false && isset($missingInvoices[$key])) {
                // remove matched invoice and missingInvoice
                unset($invoices[$invoiceIndex], $missingInvoices[$key]);
            }

            $startDate->addMonth();
        }
    }

    public function sendForcedMail($id): JsonResponse
    {
        $invoice = Invoice::find($id);

        if (!$invoice) {
            return $this->respondWithError('No invoice was found with this id.');
        }

        $mailSent = $this->sendMail($invoice);

        return is_array($mailSent)
            ? $this->respondWithSuccess($mailSent)
            : $this->respondWithError($mailSent);
    }

    public function sendForcedSms($id): JsonResponse
    {
        $invoice = Invoice::find($id);

        if (!$invoice) {
            return $this->respondWithError('No invoice was found with this id.');
        }

        $smsSent = $this->sendInvoiceSms($invoice);

        return is_array($smsSent)
            ? $this->respondWithSuccess('Sms sent to: ' . implode(', ', $smsSent))
            : $this->respondWithError($smsSent);
    }

    public function sendForcedStatementEmail($tenantId, $invoiceType = 'all'): JsonResponse
    {
        $tenant = Tenant::find($tenantId);
        $statement = $this->createStatement($tenant, 'unpaid', $invoiceType, true);

        if (!$statement['status']) {
            return $this->respondWithError($statement['message']);
        }

        $mailData = $this->getMailRecipients($tenant->contacts);

        if (!$mailData['status']) {
            return $this->respondWithError($mailData['message']);
        }

        try {
            Mail::to($mailData['recipients'])
                ->bcc($mailData['bcc'])
                ->send(new StatementMailTemplate($statement, $tenant));
        } catch (Exception $e) {
            return $this->respondWithError($e->getMessage());
        }

        $statement['tenant'] = $tenant;

        $smsSentTo = $this->sendStatementSms($statement);

        return $this->respondWithSuccess([
            'email' => $mailData['recipients'],
            'sms' => $smsSentTo
        ]);
    }

    public function sendForcedFullStatementEmail($tenantId, $paid = 'all'): JsonResponse
    {
        $tenant = Tenant::find($tenantId);
        $statement = $this->createStatement($tenant, $paid, 'all', true);

        if (!$statement['status']) {
            return $this->respondWithError($statement['message']);
        }

        $mailData = $this->getMailRecipients($tenant->contacts);

        if (!$mailData['status']) {
            return $this->respondWithError($mailData['message']);
        }

        try {
            Mail::to($mailData['recipients'])
                ->bcc($mailData['bcc'])
                ->send(new FullStatementMailTemplate($statement, $tenant));
        } catch (Exception $e) {
            return $this->respondWithError($e->getMessage());
        }

        return $this->respondWithSuccess([
            'email' => $mailData['recipients']
        ]);
    }

    public function autoGenerateRentalInvoices($latestOnly = true): JsonResponse
    {
        $missingInvoices = $this->buildMissingInvoices();
        $batchSize = config('cron.invoice-batch-size');
        $invoices = [];

        foreach ($missingInvoices as $tenantId => $data) {
            // get the latest missing invoice
            if ($latestOnly) {
                end($data);
                $data = [key($data) => current($data)];
            }

            foreach ($data as $date => $title) {
                if (0 === $batchSize) {
                    return $this->respondWithSuccess($invoices);
                }

                $request = $this->createRentalInvoiceRequest($tenantId, $date, $title);

                $invoice = new Invoice($request->all());

                /*$invoice = $this->storeInvoice($request);

                if (!($invoice instanceof Invoice)) {
                    continue;
                }

                // generate and save invoice
                $this->saveInvoicePdf($invoice);

                // send email
                $this->sendMail($invoice);
                // send sms
                $this->sendSms($invoice);*/

                $invoices[] = $invoice;
                --$batchSize;
            }
        }

        return $this->respondWithSuccess($invoices);
    }

    private function createRentalInvoiceRequest($tenantId, $date, $title): Request
    {
        $tenant = Tenant::find($tenantId);
        $date = Carbon::parse($date);

        $request = new Request();
        $request->setMethod('post');
        $request->request->add([
            'tenant_id' => $tenantId,
            'type' => 'RT',
            'amount' => $tenant->rental,
            'paid' => false,
            'year' => $date->format('y'),
            'month' => $date->format('n'),
            'created_at' => $date->format('Y-m-d') . ' 18:00:00',
            'items' => [
                [
                    'title' => $title,
                    'amount' => $tenant->rental,
                    'comment' => ''
                ]
            ],
        ]);

        return $request;
    }
}
