<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Tenant;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends JsonController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if ($validator = $this->validateData($request->all()) !== true) {
            return $this->respondWithError($validator);
        }

        $tenant = Tenant::find($request->input('tenant_id'));

        // ignore id, even if present
        $contact = $tenant->contacts()->create($request->all());

        if (!$contact) {
            return $this->respondWithError('Contact was not saved.');
        }

        return $this->respondWithSuccess($contact);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        if ($validator = $this->validateData($request->all()) !== true) {
            return $this->respondWithError($validator);
        }

        $contact = Contact::find($id);

        if (!$contact) {
            return $this->respondWithError('Cannot find contact.');
        }

        if (!$contact->fill($request->all())->save()) {
            return $this->respondWithError('Contact was not saved.');
        }

        return $this->respondWithSuccess($contact);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        if (Contact::destroy($id)) {
            return $this->respondWithSuccess($id);
        }

        return $this->respondWithError('Contact was not deleted.');
    }

    private function validateData(array $data)
    {
        $validator = Validator::make($data, [
            'tenant_id' => 'required|integer',
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:32',
            'email' => 'email|max:255|nullable',
            'notes' => 'string|max:255|nullable',
            'primary' => 'required|boolean'
        ]);

        return $validator->fails() ? $validator->errors() : true;
    }
}
