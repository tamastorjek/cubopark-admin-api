<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
use Illuminate\Http\JsonResponse;

class JsonController extends Controller
{
    /**
     * @param mixed $data
     * @return JsonResponse
     */
    protected function respondWithSuccess($data = null)
    {
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    /**
     * Accepted $messages types: string|array|MessageBag
     *
     * @param mixed $messages
     * @param int $status
     * @return JsonResponse
     */
    protected function respondWithError($messages, int $status = 404)
    {
        // convert messages to MessageBag
        if (!($messages instanceof MessageBag)) {
            if (!is_array($messages)) {
                $messages = ['default' => $messages];
            }

            $messages = new MessageBag($messages);
        }

        return response()->json(
            [
                'status' => 'error',
                'data' => null,
                'messages' => $messages
            ],
            $status
        );
    }
}
