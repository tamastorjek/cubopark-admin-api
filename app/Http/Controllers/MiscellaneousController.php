<?php

namespace App\Http\Controllers;

use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MiscellaneousController extends JsonController
{
    public function index($method, Request $request)
    {
        return $this->respondWithSuccess($this->$method($request));
    }

    private function timeDiff(Request $request)
    {
        return Carbon::parse($request->input('date'))->diffInMonths() - 1;
    }

    private function stornoFirstReading(Request $request)
    {
        $toStorno = DB::table('invoices')
            ->where([
                ['type', 'FF'],
                ['year', 18],
                ['month', 2],
                ['paid', 0]
            ])
            ->get();
    }

    public function getMapSvgString($level = 'gf', $suffix = '')
    {
        return $this->respondWithSuccess(
            File::get(resource_path(
                'assets/svg/map-' . strtoupper($level) . ($suffix ? '.' . $suffix : '') . '.svg'
            ))
        );
    }

    private function svgUnitCoords()
    {
        $units = [];
        $svg  = File::get(public_path('assets/map.svg'));

        // hotel
        $regex = '/<polygon id="([A]\d)(-)(\d+)" class="[^"]+" points="([\d\.]+) ([\d\.]+)/';

        preg_match_all($regex, $svg, $matches);

        foreach ($matches[0] as $index => $m) {
            $block = $matches[1][$index];
            $sep = $matches[2][$index];
            $unit = $matches[3][$index];

            $units[$block.$sep.$unit] = [
                'block' => $block,
                'unit' => $unit,
                'x' => $matches[4][$index],
                'y' => $matches[5][$index]
            ];
        }

        // containers, semi-d
        $regex = '/<rect id="([ABC]\d)(-)?(\d+)?" class="[^"]+" x="([\d\.]+)" y="([\d\.]+)"/';


        preg_match_all($regex, $svg, $matches);

        foreach ($matches[0] as $index => $m) {
            $block = $matches[1][$index];
            $sep = $matches[2][$index];
            $unit = $matches[3][$index];

            $units[$block.$sep.$unit] = [
                'block' => $block,
                'unit' => $unit,
                'x' => $matches[4][$index],
                'y' => $matches[5][$index]
            ];
        }

        return $units;
    }
}
