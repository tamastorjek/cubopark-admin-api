<?php

namespace App\Http\Controllers;

use App\Mail\Invoice as InvoiceMailTemplate;
use App\Models\Invoice;
use App\Models\MailRecipient;
use App\Models\Meter;
use App\Models\MeterReading;
use App\Models\Tenant;
use App\Traits\InvoiceStoreTrait;
use Barryvdh\DomPDF\Facade as PDF;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use ZipArchive;

class MeterReadingController extends JsonController
{
    use InvoiceStoreTrait;

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->respondWithSuccess(MeterReading::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tenant_id' => 'required|integer',
            'meter_id' => 'string|nullable',
            'reading' => 'required|numeric',
            'type' => 'required|string|max:1|min:1',
            // 'proof_file' => 'required|file|mimes:jpg,jpeg,png',
            'proof_file' => 'file|mimes:jpg,jpeg,png|nullable',
            'notes' => 'string|max:255|nullable',
            'created_at' => 'date_format:Y-m-d H:i:s|nullable',
            'force_invoice' => 'boolean|nullable',
            'skip_invoice' => 'boolean|nullable'
        ]);

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors());
        }

        // get tenant
        $tenant = Tenant::find($request['tenant_id']);

        $merge = [];

        // get meter
        if (!$request->filled('meter_id')) {
            $meterId = $this->getMeterIdByType($tenant, $request['type']);

            if (!$meterId) {
                return $this->respondWithError('No meter was found.');
            }

            $merge['meter_id'] = $meterId;
        } else {
            $meterId = $request['meter_id'];
        }

        // get proof file extension
        if ($request->hasFile('proof_file')) {
            $merge['proof_file_ext'] = $request->proof_file->extension();
        }

        // ignore id, even if present
        $reading = MeterReading::create(array_merge($request->all(), $merge));

        if (!$reading) {
            return $this->respondWithError('Reading was not saved.');
        }

        // save proof file
        if ($request->hasFile('proof_file')) {
            $request->proof_file->storeAs('meter-readings', $reading->file);
        }

        // update meter
        $meter = Meter::find($meterId);
        $meter->last_reading = $reading->reading;
        $meter->updated_at = $reading->created_at;

        if (!$meter->save()) {
            return $this->respondWithError('Meter was not updated.');
        }

        // skip the invoice
        if ($request->filled('skip_invoice') && $request['skip_invoice']) {
            return $this->respondWithSuccess($reading);
        }

        // don't create invoice in dev_mode
        if ($request->header('IsDevMode') === '1') {
            return $this->respondWithSuccess($reading);
        }

        // build invoice
        $invoice = $this->buildInvoice(
            $tenant,
            $reading,
            true
            // $request->filled('force_invoice') && $request['force_invoice']
        );

        if ($invoice !== true && !($invoice instanceof Invoice)) {
            $this->respondWithError($invoice);
        }

        /**
         * Generate and save invoice PDF, send email
         *
         * ONLY IF ALL METERS WERE READ
         */
        if ($invoice instanceof Invoice) {
            $this->saveInvoicePdf($invoice);
            $this->sendMail($invoice);
        }

        return $this->respondWithSuccess($reading);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get meter ID by type, return empty string if no meter was found
     *
     * @param Tenant $tenant
     * @param string $type
     * @return string
     */
    private function getMeterIdByType(Tenant $tenant, string $type): string
    {
        foreach ($tenant->units as $unit) {
            if ($unit->meters) {
                foreach ($unit->meters as $meter) {
                    if ($meter->type === $type) {
                        return $meter->id;
                    }
                }
            }
        }

        return '';
    }

    /**
     * Calculate usage and price
     *
     * @param $reading
     * @return array
     */
    private function calculateUsage($reading): array
    {
        // get the last reading before the current one
        $lastReading = MeterReading::where([
            ['id', '!=', $reading->id],
            // ['tenant_id', '=', $reading->tenant_id],
            ['meter_id', '=', $reading->meter_id]
        ])
            ->latest()
            ->first();

        // calculate consumption
        $consumption = $reading->reading - (empty($lastReading) ? 0 : $lastReading->reading);
        // calculate payable amount
        $amount = round(
            $consumption * config('payment.utility_rates.' . $reading->type),
            2
        );

        return [
            'consumption' => $consumption,
            'amount' => toInt($amount)
        ];
    }

    /**
     * Checks if the reading is invoicable (all meters for the tenant have been read!)
     *
     * @param Tenant $tenant
     * @param MeterReading $reading
     * @param bool $forceInvoice
     * @return mixed
     */
    private function buildInvoice(Tenant $tenant, MeterReading $reading, $forceInvoice = false)
    {
        // get reading date
        $readingDate = Carbon::parse($reading->created_at);

        // get total number of meters for the tenant
        $meters = 0;

        foreach ($tenant->units as $unit) {
            if ($unit->meters) {
                $meters = $meters + count($unit->meters);
            }
        }

        // get total number of readings for the tenant
        // ONLY IN THE PAST 1 WEEK
        $where = [
            ['tenant_id', '=', $tenant->id]
        ];

        if ($forceInvoice) {
            $where[] = ['created_at', 'like', explode(' ', $reading->created_at)[0] . '%'];
        } else {
            $where[] = ['created_at', '>', Carbon::today()->subWeek()];
        }

        $readings = MeterReading::where($where)
            ->orderBy('updated_at', 'desc')
            ->get();

        // not all meters have been read yet
        if ($meters !== count($readings)) {
            return true;
        }

        // calculate sum of all readings
        $amount = 0;

        foreach ($readings as $r) {
            $amount = $amount + $this->calculateUsage($r)['amount'];
        }

        // build, save and generate invoice
        $invoiceData = [
            'tenant_id' => $reading->tenant_id,
            'type' => 'FF',
            'amount' => $amount,
            'paid' => 0,
            'year' => (int)$readingDate->format('y'),
            'month' => (int)$readingDate->format('n'),
            'created_at' => $reading->created_at,
            'items' => [
                [
                    'title' => sprintf(
                        config('payment.facility_fee_invoice_items.FacilityFee'),
                        $readingDate->format('m'),
                        $readingDate->format('Y')
                    ),
                    'amount' => $amount,
                    'comment' => ''
                ]
            ]
        ];

        return $this->storeManualInvoice($invoiceData);
    }

    private function getMailRecipients($contacts): array
    {
        $response = [
            'status' => true,
            'recipients' => [],
            'bcc' => [],
            'message' => ''
        ];

        if (empty($contacts)) {
            $response['status'] = false;
            $response['message'] = 'No contacts for this tenant.';

            return $response;
        }

        // get the primary contact (or the first one if there's no primary)
        foreach ($contacts as $c) {
            if ($c->primary && !empty($c->email)) {
                $response['recipients'][] = $c;
            }
        }

        if (empty($response['recipients'])) {
            $response['status'] = false;
            $response['message'] = 'No mailable contact for this tenant.';

            return $response;
        }

        foreach (config('cubopark.email.bcc.primary') as $r) {
            $response['bcc'][] = new MailRecipient($r[0], $r[1]);
        }

        foreach (config('cubopark.email.bcc.facility') as $r) {
            $response['bcc'][] = new MailRecipient($r[0], $r[1]);
        }

        return $response;
    }

    private function sendMail(Invoice $invoice)
    {
        $mailData = $this->getMailRecipients($invoice->tenant->contacts);

        if (!$mailData['status']) {
            return $mailData['message'];
        }

        try {
            Mail::to($mailData['recipients'])
                ->bcc($mailData['bcc'])
                ->send(new InvoiceMailTemplate($invoice));

            // Log::info('Recipients: ' . implode(', ', $recipients));

            return $mailData['recipients'];
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function createReadingListPdf($tenantId): array
    {
        $tenant = Tenant::withTrashed()->find($tenantId);

        if (empty($tenant)) {
            return [
                'status' => false,
                'message' => 'Tenant not found.'
            ];
        }

        $readings = MeterReading::where('tenant_id', $tenantId)
            ->orderBy('created_at')
            ->get();

        if (empty($readings)) {
            return [
                'status' => false,
                'message' => 'No readings were found.'
            ];
        }

        $lastReading = [];

        // calculate payable amount
        foreach ($readings as &$reading) {
            if (!isset($lastReading[$reading->meter_id])) {
                $lastReading[$reading->meter_id] = 0;
            }

            $reading->usage = $reading->reading - $lastReading[$reading->meter_id];
            $reading->payable = number_format(
                round(
                    $reading->usage * config('payment.utility_rates.' . $reading->type),
                    2
                ),
                2
            );

            $lastReading[$reading->meter_id] = $reading->reading;
        }

        $path = storage_path(implode(DIRECTORY_SEPARATOR, ['app', 'meter-reading-lists']));
        $filename = 'MRL-'
            . str_pad((string)$tenant->id, 4, '0', STR_PAD_LEFT)
            . '-' . date('Ymd') . '.pdf';

        $response = [
            'status' => true,
            'path' => $path . DIRECTORY_SEPARATOR,
            'file' => $filename
        ];

        // check if reading list from same day exists
        if (File::exists($path . DIRECTORY_SEPARATOR . $filename)) {
            // return $response;
        }

        PDF::loadView(
            'manifold.pdf.meter-readings.layout',
            [
                'tenant' => $tenant,
                'date' => date('d F Y'),
                'readings' => $readings,
                'file_url' => 'api/meter-readings/proof_file/'
            ]
        )
            ->save($path . DIRECTORY_SEPARATOR . $filename);

        return $response;
    }

    public function saveReadingListPdf(Request $request, $tenantId): JsonResponse
    {
        $response = $this->createReadingListPdf($tenantId);

        if (!$response['status']) {
            return $this->respondWithError($response['message']);
        }

        return $this->respondWithSuccess($response);
    }

    public function getProofFile(Request $request, $readingId)
    {
        $reading = MeterReading::find($readingId);

        if (empty($reading)) {
            return $this->respondWithError('No reading was found.');
        }

        if (empty($reading->file)) {
            return $this->respondWithError('The reading does not have a proof file.');
        }

        $file = 'meter-readings' . DIRECTORY_SEPARATOR . $reading->file;

        if (!Storage::exists($file)) {
            return $this->respondWithError('File does not exist: ' . $file);
        }

        return response(Storage::get($file))->header('Content-Type', 'image/jpeg');
    }

    public function getAllProofFiles($tenantId)
    {
        $zipFile = storage_path(
            'app/meter-reading-archives/MeterReadings-' . $tenantId . '-' . date('Ymd') . '.zip'
        );
        $zip = new ZipArchive();

        $readings = MeterReading::where('tenant_id', $tenantId)
            ->orderBy('created_at')
            ->get();

        if (empty($readings)) {
            return [
                'status' => false,
                'message' => 'No readings were found.'
            ];
        }

        $zip->open($zipFile, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($readings as $reading) {
            if (!$reading->file) {
                continue;
            }

            $zip->addFile(storage_path('app/meter-readings/' . $reading->file), $reading->file);
        }

        $zip->close();

        return response()->download($zipFile);
    }
}
