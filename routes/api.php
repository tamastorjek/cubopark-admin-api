<?php

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// public routes
Route::post('register', 'Auth\JwtController@register');
Route::post('login', 'Auth\JwtController@login')->name('login');

Route::middleware('auth:api')->group(function () {
    // JWT
    Route::get('logout', 'Auth\JwtController@logout');
    Route::get('refresh', 'Auth\JwtController@refresh');
    Route::get('user', 'Auth\JwtController@user');

    // Config
    Route::get('config', 'ConfigController@index');

    // Units
    Route::get('units/{condition?}', 'UnitsController@index');

    // Tenants
    Route::apiResource('tenants', 'TenantController');
    Route::get('tenants/autocomplete-list', 'TenantController@autocomplete');
    Route::get('tenants/with-invoice/{type}', 'TenantController@getTenantsWithInvoice');
    Route::post('tenants/mass-store', 'TenantController@massStore');

    // Contacts
    Route::apiResource('contacts', 'ContactController');

    // Invoices
    Route::apiResource('invoices', 'InvoiceController');
    Route::get('invoices/missing/rental/{tenant_id?}', 'InvoiceController@getMissingRentalInvoices');
    Route::get('invoices/auto-generate/rt', 'InvoiceController@autoGenerateRentalInvoices');
    Route::get('invoices/email/{id}', 'InvoiceController@sendForcedMail');
    Route::get('invoices/sms/{id}', 'InvoiceController@sendForcedSms');
    Route::get('invoices/stream/{id}', 'InvoiceController@stream');
    Route::get('invoices/regenerate/tenant/{tenant_id}', 'InvoiceController@regenerateByTenant');
    Route::get('invoices/regenerate/{id}/{page?}', 'InvoiceController@regenerate');
    Route::get('invoices/update-amount-to-int', 'InvoiceController@updateDetailsAmountToInt');
    Route::get('invoices/{tenant_id}/{type}/{with_payments?}', 'InvoiceController@getByTenant');
    Route::get('statement/email/{tenant_id}/{invoice_type?}', 'InvoiceController@sendForcedStatementEmail');
    Route::get('statement/{tenant_id}/{paid?}/{invoice_type?}', 'InvoiceController@statement');
    Route::get('statement-full/email/{tenant_id}/{paid?}', 'InvoiceController@sendForcedFullStatementEmail');

    // Payments
    Route::apiResource('payments', 'PaymentController');
    Route::get('payments/regenerate/tenant/{tenant_id}', 'PaymentController@regenerateByTenant');
    Route::get('payments/regenerate/{id}/{page?}', 'PaymentController@regenerate');
    Route::get('payments/tenant/{id}', 'PaymentController@getByTenant');
    Route::get('payments/stream/{id}', 'PaymentController@stream');
    Route::get('payments/email/tenant/{id}', 'PaymentController@sendForcedMailTenant');
    Route::get('payments/email/{id}', 'PaymentController@sendForcedMail');

    // Meter readings
    Route::apiResource('meter-readings', 'MeterReadingController');
    Route::get('meter-readings/pdf/{tenant_id}', 'MeterReadingController@saveReadingListPdf');
    Route::get('meter-readings/archive-proofs/{tenant_id}', 'MeterReadingController@getAllProofFiles');

    // Meters
    Route::apiResource('meters', 'MeterController');

    // Map
    Route::get('map-svg/{level}/{opt?}', 'MiscellaneousController@getMapSvgString');
});

// retrieve meter reading proof file
Route::get('meter-readings/proof_file/{reading_id}', 'MeterReadingController@getProofFile');

// test
Route::post('misc/{func}', 'MiscellaneousController@index');
