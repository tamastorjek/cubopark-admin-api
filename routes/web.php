<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// home route
Route::get('/', function () {
    return view('angular');
});

// pdf views
Route::view('invoice', 'manifold.pdf.invoice.layout');
