<?php
$projectPath = __DIR__;

//Declare directories which contains php code
$scanDirectories = [
    $projectPath . '/app/',
    $projectPath . '/bootstrap/',
    $projectPath . '/resources/views/',
    $projectPath . '/routes/',
    $projectPath . '/config/',
];
//Optionally declare standalone files
$scanFiles = [];

return [
    'composerJsonPath' => $projectPath . '/composer.json', 'vendorPath' => $projectPath . '/vendor/',
    'scanDirectories' => $scanDirectories,
    'scanFiles' => $scanFiles
];
