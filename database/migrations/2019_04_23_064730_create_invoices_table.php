<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id')->unsigned();
            $table->char('type', 2);
            $table->integer('amount')->unsigned();
            $table->longText('items')->nullable();
            $table->boolean('paid')->default(false);
            $table->integer('paid_amount')->unsigned()->default(0);
            $table->tinyInteger('year')->unsigned();
            $table->tinyInteger('month')->unsigned();
            $table->integer('storno')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
