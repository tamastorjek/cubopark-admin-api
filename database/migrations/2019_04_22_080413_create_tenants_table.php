<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 255);
            $table->string('reg_number', 32);
            $table->string('address', 255)->nullable();
            $table->string('shop_name', 255);
            $table->integer('rental')->unsigned();
            $table->text('description')->nullable()->default(null);
            $table->char('business_type', 1);
            $table->tinyInteger('tenancy_length')->unsigned();
            $table->date('tenancy_start')->nullable()->default(null);
            $table->string('notes', 255)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
