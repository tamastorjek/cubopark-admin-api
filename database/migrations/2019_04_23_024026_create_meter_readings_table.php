<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeterReadingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meter_readings', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('tenant_id')->unsigned();
            $table->string('meter_id', 255)->nullable()->default(null);
            $table->float('reading', 10, 2)->unsigned();
            $table->char('type', 1);
            $table->string('proof_file_ext', 4);
            $table->string('notes', 255)->nullable()->default('null');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meter_readings');
    }
}
