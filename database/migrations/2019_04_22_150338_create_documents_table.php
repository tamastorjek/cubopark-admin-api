<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('tenant_id')->unsigned();
            $table->tinyInteger('type')->unsigned()->comment('1: IC, 2: SSM, 3: MISC');
            $table->string('title', 255)->nullable()->default(null);
            $table->string('extension', 4);
            $table->string('notes', 255)->nullable()->default(null);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
