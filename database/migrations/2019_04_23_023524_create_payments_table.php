<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('amount')->unsigned();
            $table->string('type', 3);
            $table->string('reference', 255)->nullable()->default(null);
            $table->string('proof_file_ext', 4)->nullable()->default(null);
            $table->string('notes', 255)->nullable()->default(null);
            $table->tinyInteger('year')->unsigned();
            $table->tinyInteger('month')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
