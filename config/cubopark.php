<?php

return [
    'company' => [
        'name' => 'MANIFOLD DIMENSIONS SDN BHD',
        'registration_number' => '1182794-H',
        'address' => '18 Lorong Halia, Tanjung Tokong, Georgetown, 10470 Pulau Pinang, Malaysia',
        'email' => 'info@manifolddimensions.com',
        'phone' => '012-4202285',
        'account_number' => '3197831617',
        'bank' => 'PUBLIC BANK BERHAD',
        'swift' => 'PBBEMYKLXXX',
        'branch' => '104, 104A & 104B, Jalan Macalister, 10400 Pulau Pinang'
    ],
    'email' => [
        'subject' => [
            'invoice' => 'Invoice %s for unit(s) %s',
            'payment' => 'Receipt %s for unit(s) %s',
            'statement' => 'Reminder: Invoice(s) owing for unit(s) %s',
            'statement_full' => 'Statement of accounts for unit(s) %s',
        ],
        'bcc' => [
            'primary' => [
                [
                    'Angie Ng',
                    'angie.ng@manifolddimensions.com'
                ],
                [
                    'Tamas Torjek',
                    'tamas.torjek@manifolddimensions.com'
                ],
            ],
            'facility' => [
                [
                    'Viki',
                    'viki@cubopark.com'
                ]
            ]
        ]
    ],
    'sms' => [
        'cc' => '60124202285',
        'invoice' => "CUBOPARK Invoice\n\nUnit(s): %s\n\nAmount: RM%s\n\nDue date: %s"
            . "\n\nInvoice sent by email\nThank you",
        'statement' => "CUBOPARK Reminder\n\nInvoice(s) owing for unit(s) %s\n\nAmount: RM%s"
            . "\n\nDue date: %s\n\nStatement sent by email\nThank you"
    ]
];
