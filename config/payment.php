<?php

return [
    'base_prefix' => 'MD/',
    'invoice_prefix' => 'IN/',
    'receipt_prefix' => 'PR/',

    'types' => [
        'BT' => 'Bank transfer',
        'CHQ' => 'Cheque',
        'CD' => 'Cash deposit',
        'C' => 'Cash'
    ],

    'invoice_types' => [
        'RT' => 'Rental',
        'FF' => 'Facility fee',
        'PE' => 'Penalty',
        'TA' => 'Deposits',
        'CT' => 'Custom',
        'ST' => 'Storno'
    ],

    // <get>METHOD => TEXT
    'tenancy_invoice_items' => [
        'AdvanceRental' => 'Advance rental (1 month)',
        'SecurityDeposit' => 'Security deposit (2 months)',
        'FacilityFee' => 'Facility fee deposit (half month)',
        'UtilityMeter' => 'Utility meters deposit',
        'Lawyer' => 'Stamping and lawyer fees'
    ],

    'facility_fee_invoice_items' => [
        'FacilityFee' => 'Facility fee (%s/%s)'
    ],

    'rental_invoice_items' => [
        'title' => 'Rent for the period of '
    ],

    'utility_rates' => [
        'E' => 0.6, // 1kW
        'W' => 1.3 // 1,000L (1 cubic meter)
    ]
];
